//
//  AppDelegate.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 11/12/19.
//  Copyright © 2019 Navi. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import RealmSwift
import IQKeyboardManager


var uiRealm = try! Realm()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()

              let db = Firestore.firestore()
              print (db)
              
              IQKeyboardManager.shared().isEnabled = true
              IQKeyboardManager.shared().isEnableAutoToolbar = false
              IQKeyboardManager.shared().shouldResignOnTouchOutside = true
              

        
        //API key of AccommodationApp:AIzaSyAAx6KIrG3hrG1GsNh7uEbpK5KP3VOnHwI//
        
        GMSServices.provideAPIKey("AIzaSyAotSWXVZ_zuVVI29oRDMMwcSEk4wXaJK0")
        GMSPlacesClient.provideAPIKey("AIzaSyAotSWXVZ_zuVVI29oRDMMwcSEk4wXaJK0")
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

