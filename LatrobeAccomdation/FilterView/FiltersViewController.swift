//
//  FiltersViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 29/12/19.
//  Copyright © 2019 Navi. All rights reserved.
//

import UIKit
import UISliderTwoSide

class FiltersViewController: UIViewController, UISliderTwoSideDelegate{
    
    //    --------------Property Type Buttons-----------------
    @IBOutlet weak var apartmentButton: UIButton!
    @IBOutlet weak var studioAptButton: UIButton!
    @IBOutlet weak var sharedRoomButton: UIButton!
    @IBOutlet weak var privateRoomButton: UIButton!
    //    --------------Furnished Buttons-----------------
    @IBOutlet weak var bedsButton: UIButton!
    @IBOutlet weak var studyTableButton: UIButton!
    @IBOutlet weak var wardrobeButton: UIButton!
    @IBOutlet weak var kitchenAmenitiesButton: UIButton!
    //    --------------Features Buttons-----------------
    @IBOutlet weak var roomsButton: UIButton!
    @IBOutlet weak var bathroomsButton: UIButton!
    @IBOutlet weak var coolingButton: UIButton!
    @IBOutlet weak var wifiButton: UIButton!
    @IBOutlet weak var washingMachineButton: UIButton!
    @IBOutlet weak var heatingButton: UIButton!
    @IBOutlet weak var parkingButton: UIButton!
    @IBOutlet weak var wheelchairButton: UIButton!
    //    --------------Accessibilities Buttons-----------------
    @IBOutlet weak var publicTransportButton: UIButton!
    @IBOutlet weak var storeButton: UIButton!
    @IBOutlet weak var shoppingButton: UIButton!
    @IBOutlet weak var restaurantButton: UIButton!
    @IBOutlet weak var swimmingButton: UIButton!
    @IBOutlet weak var hospitalButton: UIButton!
    @IBOutlet weak var fuelStationButton: UIButton!
    @IBOutlet weak var gymButton: UIButton!
    //    --------------Tenant Buttons-----------------
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var noPreferenceButton: UIButton!
    //    --------------HomeRules Buttons-----------------
    @IBOutlet weak var petFriendlyButton: UIButton!
    @IBOutlet weak var noSmokingButton: UIButton!
    @IBOutlet weak var quitePlaceButton: UIButton!
    
    @IBOutlet weak var rentMinValue: UILabel!
    
    @IBOutlet weak var rentMaxValue: UILabel!
    
    @IBOutlet weak var slider: UISliderTwoSide! {
        didSet {
            slider.delegate = self
        }
    }
    
    
    
    
    //    --------------Property Type variables-----------------
    var apartmentFlipped: Bool = false
    var studioAptFlipped: Bool = false
    var sharedRoomFlipped: Bool = false
    var privateRoomFlipped: Bool = false
    //    --------------Furnished Variables-----------------
    var bedsFlipped : Bool = false
    var studyTableFlipped : Bool = false
    var wardrobeFlipped : Bool = false
    var kitchenAmenitiesFlipped : Bool = false
    //    --------------Features Variables-----------------
    var roomsFlipped : Bool = false
    var bathroomsFlipped : Bool = false
    var coolingFlipped : Bool = false
    var wifiFlipped : Bool = false
    var washingMachineFlipped : Bool = false
    var heatingFlipped : Bool = false
    var parkingFlipped : Bool = false
    var wheelchairFlipped : Bool = false
    //    --------------Accessibilities Variables-----------------
    var publicTransportFlipped : Bool = false
    var storeFlipped : Bool = false
    var shoppingFlipped : Bool = false
    var restaurantFlipped : Bool = false
    var swimmingFlipped : Bool = false
    var hospitalFlipped : Bool = false
    var fuelstationFlipped : Bool = false
    var gymFlipped : Bool = false
    //    --------------Tenants Variables-----------------
    var maleFlipped: Bool = false
    var femaleFlipped: Bool = false
    var noPreferenceFlipped: Bool = false
    //    --------------HomeRules Buttons-----------------
    var petFriendlyFlipped: Bool = false
    var noSmokingFlipped: Bool = false
    var quitePlaceFlipped: Bool = false
    
    //    --------------Property Type Variables Data-----------------
    var apartmentData: Int = 1
    var studioAptData: Int = 1
    var sharedRoomData: Int = 1
    var privateRoomData: Int = 1
    //    --------------Furnished Variables Data-----------------
    var bedsData : Int = 1
    var studyTableData : Int = 1
    var wardrobeData : Int = 1
    var kitchenAmenitiesData : Int = 1
    //    --------------Features Variables Data-----------------
    var roomsData : Int = 1
    var bathroomsData : Int = 1
    var coolingData : Int = 1
    var wifiData : Int = 1
    var washingMachineData : Int = 1
    var heatingData : Int = 1
    var parkingData : Int = 1
    var wheelchairData : Int = 1
    //    --------------Accessibilities Variables Data-----------------
    var publicTransportData : Int = 1
    var storeData : Int = 1
    var shoppingData : Int = 1
    var restaurantData : Int = 1
    var swimmingData : Int = 1
    var hospitalData : Int = 1
    var fuelstationData : Int = 1
    var gymData : Int = 1
    //    --------------Tenants Variables Data-----------------
    var maleData: Int = 1
    var femaleData: Int = 1
    var noPreferenceData: Int = 1
    //    --------------HomeRules Variable Data-----------------
    var petFriendlyData: Int = 1
    var noSmokingData: Int = 1
    var quitePlaceData: Int = 1
    
   
    var minRentValue: Int = 0
    var maxRentValue: Int = 2000
    
    
    @IBOutlet weak var saveButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        apartmentFlipped = (UserDefaults.standard.object(forKey: "apartmentFlipped") != nil)
//        print(apartmentFlipped)
        saveButton.layer.cornerRadius = 10
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func sliderTwoSide(slider: UISliderTwoSide, minValue: CGFloat, maxValue: CGFloat) {
    //        print("min: \(minValue)  |  max: \(maxValue)")
            minRentValue = Int(minValue*2000)
            maxRentValue = Int(maxValue*2000)
            
            rentMinValue.text = "Min :$\(minRentValue)"
            rentMaxValue.text = "Max :$\(maxRentValue)"
        }
    
    
       //    --------------Property Type Buttons Pressed-----------------
    
    @IBAction func apartmentButtonTapped(_ sender: Any) {
//        print("apatment tapped")
        apartmentFlipped.toggle()
        
        if apartmentFlipped{
            apartmentButton.tintColor = UIColor.label
            apartmentData = 0
//            print(apartmentFlipped)
            
        }else{
            apartmentButton.tintColor = UIColor.lightGray
            apartmentData = 1
//            print(apartmentFlipped)
            
            
        }
    }
    
    @IBAction func studioAptButtonTapped(_ sender: Any) {
        
        studioAptFlipped.toggle()
        
        if studioAptFlipped{
            studioAptButton.tintColor = UIColor.label
            studioAptData = 0
           
        }else{
            studioAptButton.tintColor = UIColor.lightGray
            studioAptData = 1
            
            
        }
    }
    
    @IBAction func sharedRoomButtonTapped(_ sender: Any) {
        
        sharedRoomFlipped.toggle()
        
        if sharedRoomFlipped{
            sharedRoomButton.tintColor = UIColor.label
            sharedRoomData = 0
            
        }else{
            sharedRoomButton.tintColor = UIColor.lightGray
            sharedRoomData = 1
            
        }
    }
    
    @IBAction func privateRoomButtonTapped(_ sender: Any) {
        
        privateRoomFlipped.toggle()
        
        if privateRoomFlipped{
            privateRoomButton.tintColor = UIColor.label
            privateRoomData = 0
            
        }else{
            privateRoomButton.tintColor = UIColor.lightGray
            privateRoomData = 1
            
        }
    }
    
    
    
    
    //    --------------Furnished Buttons Pressed-----------------
    
    @IBAction func bedsButtonTapped(_ sender: Any) {
        
        bedsFlipped.toggle()
        
        if bedsFlipped{
            bedsButton.tintColor = UIColor.label
            bedsData = 0
            
        }else{
            bedsButton.tintColor = UIColor.lightGray
            bedsData = 1
            
        }
    }
    @IBAction func studyTableButtonTapped(_ sender: Any) {
        
        studyTableFlipped.toggle()
        
        if studyTableFlipped{
            studyTableButton.tintColor = UIColor.label
            studyTableData = 0
            
        }else{
            studyTableButton.tintColor = UIColor.lightGray
            studyTableData = 1
            
        }
        
    }
    
    @IBAction func wardrobeButtonTapped(_ sender: Any) {
        
        wardrobeFlipped.toggle()
        
        if wardrobeFlipped{
            wardrobeButton.tintColor = UIColor.label
            wardrobeData = 0
            
        }else{
            wardrobeButton.tintColor = UIColor.lightGray
            wardrobeData = 1
            
        }
        
    }
    
    @IBAction func kitchenAmenitiesButtonTapped(_ sender: Any) {
        
        kitchenAmenitiesFlipped.toggle()
        
        if kitchenAmenitiesFlipped{
            kitchenAmenitiesButton.tintColor = UIColor.label
            kitchenAmenitiesData = 0
            
        }else{
            kitchenAmenitiesButton.tintColor = UIColor.lightGray
            kitchenAmenitiesData = 1
                    }
    }
    
    
    //    --------------Features Buttons Pressed-----------------
    
    @IBAction func roomsButtonTapped(_ sender: Any) {
        
        roomsFlipped.toggle()
        
        if roomsFlipped{
            roomsButton.tintColor = UIColor.label
            roomsData = 0
            
        }else{
            roomsButton.tintColor = UIColor.lightGray
            roomsData = 1
            
        }
    }
    
    @IBAction func bathroomsButtonTapped(_ sender: Any) {
        
        bathroomsFlipped.toggle()
        
        if bathroomsFlipped{
            bathroomsButton.tintColor = UIColor.label
            bathroomsData = 0
            
        }else{
            bathroomsButton.tintColor = UIColor.lightGray
            bathroomsData = 1
            
        }
    }
    
    @IBAction func coolingButtonTapped(_ sender: Any) {
        
        coolingFlipped.toggle()
        
        if coolingFlipped{
            coolingButton.tintColor = UIColor.label
            coolingData = 0
            
        }else{
            coolingButton.tintColor = UIColor.lightGray
            coolingData = 1
            
        }
    }
    
    @IBAction func wifiButtonTapped(_ sender: Any) {
        
        wifiFlipped.toggle()
        
        if wifiFlipped{
            wifiButton.tintColor = UIColor.label
            wifiData = 0
            
        }else{
            wifiButton.tintColor = UIColor.lightGray
            wifiData = 1
            
        }
    }
    
    @IBAction func washingMachineButtonTapped(_ sender: Any) {
        
        washingMachineFlipped.toggle()
        
        if washingMachineFlipped{
            washingMachineButton.tintColor = UIColor.label
            washingMachineData = 0
            
        }else{
            washingMachineButton.tintColor = UIColor.lightGray
            washingMachineData = 1
            
        }
    }
    
    @IBAction func heatingButtonTapped(_ sender: Any) {
        
        heatingFlipped.toggle()
        
        if heatingFlipped{
            heatingButton.tintColor = UIColor.label
            heatingData = 0
            
        }else{
            heatingButton.tintColor = UIColor.lightGray
            heatingData = 1
            
        }
    }
    
    @IBAction func parkingButtonTapped(_ sender: Any) {
        
        parkingFlipped.toggle()
        
        if parkingFlipped{
            parkingButton.tintColor = UIColor.label
            parkingData = 0
            
        }else{
            parkingButton.tintColor = UIColor.lightGray
            parkingData = 1
            
        }
    }
    
    @IBAction func wheelchairButtonTapped(_ sender: Any) {
        
        wheelchairFlipped.toggle()
        
        if wheelchairFlipped{
            wheelchairButton.tintColor = UIColor.label
            wheelchairData = 0
            
        }else{
            wheelchairButton.tintColor = UIColor.lightGray
            wheelchairData = 1
            
        }
        
        
    }
    
    
    //    --------------Accessibilities Buttons Pressed-----------------
    
    @IBAction func publicTransportButtonTapped(_ sender: Any) {
        
        publicTransportFlipped.toggle()
        
        if publicTransportFlipped{
            publicTransportButton.tintColor = UIColor.label
            publicTransportData = 0
            
        }else{
            publicTransportButton.tintColor = UIColor.lightGray
            publicTransportData = 1
            
        }
    }
    
    @IBAction func storeButtonTapped(_ sender: Any) {
        
        storeFlipped.toggle()
        
        if storeFlipped{
            storeButton.tintColor = UIColor.label
            storeData = 0
            
        }else{
            storeButton.tintColor = UIColor.lightGray
            storeData = 1
            
        }
    }
    
    @IBAction func shoppingButtonTapped(_ sender: Any) {
        
        shoppingFlipped.toggle()
        
        if shoppingFlipped{
            shoppingButton.tintColor = UIColor.label
            shoppingData = 0
            
        }else{
            shoppingButton.tintColor = UIColor.lightGray
            shoppingData = 1
            
        }
    }
    
    @IBAction func restaurantButtonTapped(_ sender: Any) {
        
        restaurantFlipped.toggle()
        
        if restaurantFlipped{
            restaurantButton.tintColor = UIColor.label
            restaurantData = 0
            
        }else{
            restaurantButton.tintColor = UIColor.lightGray
            restaurantData = 1
            
        }
    }
    
    
    @IBAction func swimmingButtonTapped(_ sender: Any) {
        
        swimmingFlipped.toggle()
        
        if swimmingFlipped{
            swimmingButton.tintColor = UIColor.label
            swimmingData = 0
            
        }else{
            swimmingButton.tintColor = UIColor.lightGray
            swimmingData = 1
            
        }
    }
    
    
    @IBAction func hospitalButtonTapped(_ sender: Any) {
        
        hospitalFlipped.toggle()
        
        if hospitalFlipped{
            hospitalButton.tintColor = UIColor.label
            hospitalData = 0
            
            
        }else{
            hospitalButton.tintColor = UIColor.lightGray
            hospitalData = 1
           
        }
    }
    
    
    @IBAction func fuelButtonTapped(_ sender: Any) {
        
        fuelstationFlipped.toggle()
        
        if fuelstationFlipped{
            fuelStationButton.tintColor = UIColor.label
            fuelstationData = 0
            
        }else{
            fuelStationButton.tintColor = UIColor.lightGray
            fuelstationData = 1
            
        }
    }
    
    @IBAction func gymButtonTapped(_ sender: Any) {
        
        gymFlipped.toggle()
        
        if gymFlipped{
            gymButton.tintColor = UIColor.label
            gymData = 0
            
        }else{
            gymButton.tintColor = UIColor.lightGray
            gymData = 1
            
        }
    }
    
    
    
    //    --------------Tenants Buttons Pressed-----------------
    @IBAction func maleButtonTapped(_ sender: Any) {
        
        maleFlipped.toggle()
        
        if maleFlipped{
            maleButton.tintColor = UIColor.label
            maleData = 0
            
        }else{
            maleButton.tintColor = UIColor.lightGray
            maleData = 1
            
        }
    }
    
    @IBAction func femaleButtonTapped(_ sender: Any) {
        
        femaleFlipped.toggle()
        
        if femaleFlipped{
            femaleButton.tintColor = UIColor.label
            femaleData = 0
            
        }else{
            femaleButton.tintColor = UIColor.lightGray
            femaleData = 1
            
        }
    }
    
    @IBAction func noPreferenceButtonTapped(_ sender: Any) {
        
        noPreferenceFlipped.toggle()
        
        if noPreferenceFlipped{
            noPreferenceButton.tintColor = UIColor.label
            noPreferenceData = 0
            
        }else{
            noPreferenceButton.tintColor = UIColor.lightGray
            noPreferenceData = 1
            
            
        }
    }
    
    
    //    --------------HomeRules Buttons Pressed-----------------
    @IBAction func petFriendlyButtonTapped(_ sender: Any) {
        
        
        petFriendlyFlipped.toggle()
        
        if petFriendlyFlipped{
            petFriendlyButton.tintColor = UIColor.label
            petFriendlyData = 0
            
            
        }else{
            petFriendlyButton.tintColor = UIColor.lightGray
            petFriendlyData = 1
            
        }
    }
    
    
    @IBAction func noSmokingButtonTapped(_ sender: Any) {
        
        noSmokingFlipped.toggle()
        
        if noSmokingFlipped{
            noSmokingButton.tintColor = UIColor.label
            noSmokingData = 0
            
        }else{
            noSmokingButton.tintColor = UIColor.lightGray
            noSmokingData = 1
            
        }
    }
    
    @IBAction func quitePlaceButtonTapped(_ sender: Any) {
        
        quitePlaceFlipped.toggle()
        
        if quitePlaceFlipped{
            quitePlaceButton.tintColor = UIColor.label
            quitePlaceData = 0
            
        }else{
            quitePlaceButton.tintColor = UIColor.lightGray
            quitePlaceData = 1
            
        }
    }
    
    
    
    @IBAction func saveButton(_ sender: Any) {
//        UserDefaults.standard.set(apartmentFlipped, forKey: "apartmentFlipped")
        
        
        
        performSegue(withIdentifier: "addingFiltersToSearch", sender: self)
        self.dismiss(animated: true, completion: nil)
                
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! SearchResultsViewController
        controller.apartmentData = apartmentData
        controller.studioAptData = studioAptData
        controller.sharedRoomData = sharedRoomData
        controller.privateRoomData = privateRoomData
        controller.bedsData = bedsData
        controller.studyTableData = studyTableData
        controller.wardrobeData = wardrobeData
        controller.kitchenAmenitiesData = kitchenAmenitiesData
        controller.roomsData = roomsData
        controller.bathroomsData = bathroomsData
        controller.coolingData = coolingData
        controller.wifiData = wifiData
        controller.washingMachineData = washingMachineData
        controller.heatingData = heatingData
        controller.parkingData = parkingData
        controller.wheelchairData = wheelchairData
        controller.publicTransportData = publicTransportData
        controller.storeData = storeData
        controller.shoppingData = shoppingData
        controller.restaurantData = restaurantData
        controller.swimmingData = swimmingData
        controller.hospitalData = hospitalData
        controller.fuelstationData = fuelstationData
        controller.gymData = gymData
        controller.maleData = maleData
        controller.femaleData = femaleData
        controller.noPreferenceData = noPreferenceData
        controller.petFriendlyData = petFriendlyData
        controller.noSmokingData = noSmokingData
        controller.quitePlaceData = quitePlaceData
        controller.maxRentValue = maxRentValue
        controller.minRentValue = minRentValue

        

    }
    
}


