
//
//  ViewController.swift
//  Custom MKAnnotation with Image
//
//  Created by TIANYI LI on 23/1/20.
//  Copyright © 2020 TIANYI LI. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
//no need to use coreloaction to get the user's current location


//first, build a new class for annotation with image
//MKannotathion - info about the location on a map
//In our case, the location's info includes: the coordiantes, title, subtitle, an image for the location and the color(don't really know this colour is for)


//give the type of each property: coordinates/title/subtitle/image/colour
class ImageAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var colour: UIColor?

    
    //give the default value of each property
    override init() {
        self.coordinate = CLLocationCoordinate2D()
        self.title = nil
        self.subtitle = nil
        self.image = nil
        self.colour = UIColor.white

        
        
        
        
    }
}

//2nd, build a class for annotation View for this imageAnnotation
//MKAnnotation (MKAnnotationPoint) is completely different from MKAnnotationView. MKAnnotation holds information about the location on a map. **MKAnnotationView decides how it appears or looks.**
class ImageAnnotationView: MKAnnotationView {
   
    private var imageView: UIImageView!
    //private var means, you can only access to imageView within the class: ImageAnnotationView
    //private restricts the access. force people to only use the intended, and reduce error
    
    
    //this sets how the func MKannotationView should be strucuted
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        
        //setting properties
        self.frame = CGRect(x: 0, y: 0, width: 50, height: 50) //50*50 frame -- changing the width and height doesn't make any difference :(
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        // The image is also 50*50 -- > the size of the image <
        
        //add this imageView to MKannotationView -- this is how you add the image to the annotation
        self.addSubview(self.imageView)

        //set the properties of the imageView
        self.imageView.layer.cornerRadius = 5.0
        
        self.imageView.layer.masksToBounds = true
        //masksToBounds: no idea what the fuck this is
    }

    
    //????? no very clear, but probably give the imageView
    override var image: UIImage? {
        get {
            return self.imageView.image
        }

        set {
            self.imageView.image = newValue
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class GoogleMapsViewController: UIViewController, MKMapViewDelegate {

    var mapView: MKMapView!
    var locationManager: CLLocationManager!
    
    var locations = [["price": "$445", "title": "107/94 Canning St Carlton 3053", "suburb": "Carlton 3053" ,"lat" : 12.989415,
                      "long" : 80.216386, "url" : "https://i.imgur.com/zIoAyCx.png"],
                     ["price": "$568","title": "48/188 Walls Street Dockland 3030", "suburb": "Dockland 3030" ,"lat" : 12.988445,
     "long" : 80.218491, "url" :  "https://i.imgur.com/MedV8nk.jpg"],
                     ["price": "$360", "title": "901/488 Swanston St Carlton 3050", "suburb": "Carlton 3050", "lat" : 12.983192,
     "long" : 80.219922, "url" : "https://i.imgur.com/cgS8226.jpg"],
                     ["price": "$390", "title": "10/20 Token Revenue Coburg North 3000", "suburb": "Coburg North 3000", "lat" : 12.982892,
     "long" : 80.216922, "url" : "https://i.imgur.com/Bi5TU9U.jpg"],
                     ["price": "$491", "title": "252 Jonston Street Fitzroy 3079", "suburb": "Fitzroy 3079", "lat" : 12.987192,
     "long" : 80.219422, "url" : "https://i.imgur.com/Uw5BAwF.jpg"]]
    
    var meanPriceInArea = 400
    
    

    override func viewDidLoad() {
        
        
        
        
        super.viewDidLoad()
    
        

        self.initControls()
        self.doLayout()
        self.loadAnnotations()
        
        
        
        
        
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func initControls() {
        self.mapView = MKMapView()

        self.mapView.isRotateEnabled = true
        self.mapView.showsUserLocation = true
        self.mapView.delegate = self

        
        
        
        let center = CLLocationCoordinate2D(latitude: 12.988, longitude: 80.218)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008)
        
        let region = MKCoordinateRegion(center: center, span: span)
        self.mapView.setRegion(region, animated: true)
    }

    func doLayout() {
        self.view.addSubview(self.mapView)
        self.mapView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.mapView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.mapView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) {  //Handle user location annotation..
            return nil  //Default is to let the system handle it.
        }

        if !annotation.isKind(of: ImageAnnotation.self) {  //Handle non-ImageAnnotations..
            var pinAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "DefaultPinView")
            if pinAnnotationView == nil {
                pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "DefaultPinView")
            }
            return pinAnnotationView
        }

        //Handle ImageAnnotations..
        var view: ImageAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "imageAnnotation") as? ImageAnnotationView
        if view == nil {
            view = ImageAnnotationView(annotation: annotation, reuseIdentifier: "imageAnnotation")
        }

        let annotation = annotation as! ImageAnnotation
        view?.image = annotation.image
        view?.annotation = annotation
        view?.canShowCallout = true
        
        
        
        let price = UILabel(frame: CGRect(x: -5, y: -25, width: 60, height: 42))
        
        
        /*for element in locations {
            let price = element["price"]!
            let priceInt = Int(
            if price <
        }*/
        
        
        
        price.textColor = .red
        
        price.tag = 42
        price.textAlignment = .center
        price.text = annotation.title!
        
        price.font = .boldSystemFont(ofSize: 18)
        price.shadowColor = .white
        price.layer.shadowRadius = 3
        price.layer.shadowOpacity = 0.7
        price.layer.shadowOffset = CGSize(width: 2, height: 2)
        
        view?.addSubview(price)
        view?.viewWithTag(42)
        
        

        return view
    }


    /*func loadAnnotations() {
        let request = NSMutableURLRequest(url: URL(string: "https://i.imgur.com/zIoAyCx.png")!)
        request.httpMethod = "GET"

        let session = URLSession(configuration: URLSessionConfiguration.default)
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            if error == nil {

                let annotation = ImageAnnotation()
                annotation.coordinate = CLLocationCoordinate2DMake(43.761539, -79.411079)
                annotation.image = UIImage(data: data!, scale: UIScreen.main.scale)
                annotation.title = "Toronto"
                annotation.subtitle = "Yonge & Bloor"



                DispatchQueue.main.async {
                    self.mapView.addAnnotation(annotation)
                }
            }
        }

        dataTask.resume()
    }*/
    
    func loadAnnotations() {

        for address in locations {
            
          
            
            
            DispatchQueue.main.async {
                let request = NSMutableURLRequest(url: URL(string: address["url"] as! String)!)
                request.httpMethod = "GET"
                let session = URLSession(configuration: URLSessionConfiguration.default)
                let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
                    if error == nil {

                        let annotation = ImageAnnotation()
                        annotation.coordinate = CLLocationCoordinate2D (latitude: address["lat"] as! CLLocationDegrees, longitude: address["long"] as! CLLocationDegrees)
                        
                        annotation.image = UIImage(data: data!, scale: UIScreen.main.scale)
                        
                        
                        annotation.title = address["price"] as? String
                        annotation.subtitle = address["title"] as? String
                        
                        
                        
                        DispatchQueue.main.async {
                            self.mapView.addAnnotation(annotation)
                        }
                    }
                }

                dataTask.resume()
            }
        }
    }
    
    
    
    //
}
