//
//  Constant.swift
//  LatrobeAccomdation
//
//  Created by lorena munoz on 22/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import Foundation

struct K {
    static let appName = "La Trobe Accommodation"
    static let cellIdentifier = "ReusableCell"
    static let cellNibName = "MessageCell"
    static let registerSegue = "RegisterToChat"
    static let loginSegue = "LoginToChat"
    
struct BrandColors {
        static let red = "RedColorLaTrobe"
        static let darkWhite = "DarkWhiteColorLaTrobe"
        static let black = "BlackColorLaTrobe"
        static let lightRed = "LightRedColorLaTrobe"
    }
    
    struct FStore {
        static let collectionName = "messages"
        static let senderField = "sender"
        static let bodyField = "body"
        static let dateField = "date"
    }
}
