//
//  ViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 11/12/19.
//  Copyright © 2019 Navi. All rights reserved.
//

import UIKit
import CLTypingLabel
import DropdownButton


class ViewController: UIViewController {
    
    @IBOutlet weak var searchButton: UIButton!
    
//    @IBOutlet weak var latrobeLabel: CLTypingLabel!
    
    @IBOutlet weak var dropDown: DropdownButton!
    
    let valueLabel = UILabel()
    var selectedCapmus:String = ""
    
    let options = ["Select Campus","Albury-Wodonga", "Bundoora", "Bendigo", "Mildura", "Shepparton", "Sydney"]
    let ids = [0, 1, 2,3,4,5,6,7]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
//        latrobeLabel.text = "Latrobe Accomdation"
        
        //DROPDWN --
        dropDown.layer.cornerRadius = 10
//        dropDown.backgroundColor = .
        dropDown.setTitleColor(.white, for: .normal)
        dropDown.selectedRowColor = .lightGray
//        dropDown.selectedRowTextColor = .white
        dropDown.rowHeight = 40
        
        
        
        dropDown.didSelect { (selectedText , index ,id) in
            self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
            self.selectedCapmus = selectedText
            if self.selectedCapmus != "" {
                self.performSegue(withIdentifier: "toSearchResultsView", sender: self)
            print(self.selectedCapmus)
            }
        }
        
        // seting options
        
        
        dropDown.setOptions(options: options, optionIds: ids, selectedIndex: 0)
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dropDown.setOptions(options: options, optionIds: ids, selectedIndex: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        dropDown.setOptions(options: options, optionIds: ids, selectedIndex: 0)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let resultpage = segue.destination as! SearchResultsViewController
        resultpage.selectedCapmus = selectedCapmus
        print("Sending campus \(selectedCapmus)")
        
    }

    
}
