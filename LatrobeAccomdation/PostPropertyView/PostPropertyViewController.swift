//
//  PostPropertyViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 4/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase
import DropdownButton
import GooglePlaces
import GoogleMaps
import DateTextField
import WTStepper

class PostPropertyViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate, UITextViewDelegate{
    
    
    
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var propertyImage: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var addressTextField: UILabel!
    
    
    @IBOutlet weak var refreshAddress: UIButton!
    
    @IBOutlet weak var rentSlider: UISlider!
    @IBOutlet weak var rentLabel: UILabel!
    
    @IBOutlet weak var billsIncluded: UILabel!
    
    @IBOutlet weak var swiftBillsIncluded: UISwitch!
    
    @IBOutlet weak var stayLength: DropdownButton!
    
    @IBOutlet weak var maximumStayLength: DropdownButton!
    
    @IBOutlet weak var dropDown: DropdownButton!
    
    @IBOutlet weak var dropDownFrom: DropdownButton!
    
    
    @IBOutlet weak var dropdownPTV: DropdownButton!
    
    //    --------------Property Type Buttons-----------------
    @IBOutlet weak var apartmentButton: UIButton!
    @IBOutlet weak var studioAptButton: UIButton!
    @IBOutlet weak var sharedRoomButton: UIButton!
    @IBOutlet weak var privateRoomButton: UIButton!
    //    --------------Furnished Buttons-----------------
    @IBOutlet weak var bedsButton: UIButton!
    @IBOutlet weak var studyTableButton: UIButton!
    @IBOutlet weak var wardrobeButton: UIButton!
    @IBOutlet weak var kitchenAmenitiesButton: UIButton!
    //    --------------Features Buttons-----------------
    @IBOutlet weak var roomsButton: UIButton!
    @IBOutlet weak var bathroomsButton: UIButton!
    @IBOutlet weak var coolingButton: UIButton!
    @IBOutlet weak var wifiButton: UIButton!
    @IBOutlet weak var washingMachineButton: UIButton!
    @IBOutlet weak var heatingButton: UIButton!
    @IBOutlet weak var parkingButton: UIButton!
    @IBOutlet weak var wheelchairButton: UIButton!
    //    --------------Accessibilities Buttons-----------------
    @IBOutlet weak var publicTransportButton: UIButton!
    @IBOutlet weak var storeButton: UIButton!
    @IBOutlet weak var shoppingButton: UIButton!
    @IBOutlet weak var restaurantButton: UIButton!
    @IBOutlet weak var swimmingButton: UIButton!
    @IBOutlet weak var hospitalButton: UIButton!
    @IBOutlet weak var fuelStationButton: UIButton!
    @IBOutlet weak var gymButton: UIButton!
    //    --------------Tenant Buttons-----------------
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var noPreferenceButton: UIButton!
    //    --------------HomeRules Buttons-----------------
    @IBOutlet weak var petFriendlyButton: UIButton!
    @IBOutlet weak var noSmokingButton: UIButton!
    @IBOutlet weak var quitePlaceButton: UIButton!
    
    @IBOutlet weak var addressMapMarker: GMSMapView!
    @IBOutlet weak var searchAddress: UIButton!
    @IBOutlet weak var availableFromTextField: UITextField!
    @IBOutlet weak var inspectionFromTextField: UITextField!
    
    @IBOutlet weak var characterLabel: UILabel!
    
    
    @IBOutlet weak var propertyTitleLabel: UILabel!
    @IBOutlet weak var propertyTitleTextField: UITextField!
    
    @IBOutlet weak var detailPropertyLabel: UILabel!
    @IBOutlet weak var propertyDetailTextView: UITextView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var bedStepper: WTStepper!
    
    @IBOutlet weak var bedDetailsText: UITextField!
    
    //------------------ date variables-----
    
    var availableDate = Date()
    
    //------------------ bill variables
    var billSwitchOn: Bool = false
    //    --------------Property Type variables-----------------
    var apartmentFlipped: Bool = false
    var studioAptFlipped: Bool = false
    var sharedRoomFlipped: Bool = false
    var privateRoomFlipped: Bool = false
    //    --------------Furnished Variables-----------------
    var bedsFlipped : Bool = false
    var studyTableFlipped : Bool = false
    var wardrobeFlipped : Bool = false
    var kitchenAmenitiesFlipped : Bool = false
    //    --------------Features Variables-----------------
    var roomsFlipped : Bool = false
    var bathroomsFlipped : Bool = false
    var coolingFlipped : Bool = false
    var wifiFlipped : Bool = false
    var washingMachineFlipped : Bool = false
    var heatingFlipped : Bool = false
    var parkingFlipped : Bool = false
    var wheelchairFlipped : Bool = false
    //    --------------Accessibilities Variables-----------------
    var publicTransportFlipped : Bool = false
    var storeFlipped : Bool = false
    var shoppingFlipped : Bool = false
    var restaurantFlipped : Bool = false
    var swimmingFlipped : Bool = false
    var hospitalFlipped : Bool = false
    var fuelstationFlipped : Bool = false
    var gymFlipped : Bool = false
    //    --------------Tenants Variables-----------------
    var maleFlipped: Bool = false
    var femaleFlipped: Bool = false
    var noPreferenceFlipped: Bool = false
    //    --------------HomeRules Variables-----------------
    var petFriendlyFlipped: Bool = false
    var noSmokingFlipped: Bool = false
    var quitePlaceFlipped: Bool = false
    
    //------- bill variable data-----
    var billData : Int = 1
    //    --------------Property Type Variables Data-----------------
    var apartmentData: Int = 1
    var studioAptData: Int = 1
    var sharedRoomData: Int = 1
    var privateRoomData: Int = 1
    //    --------------Furnished Variables Data-----------------
    var bedsData : Int = 1
    var studyTableData : Int = 1
    var wardrobeData : Int = 1
    var kitchenAmenitiesData : Int = 1
    //    --------------Features Variables Data-----------------
    var roomsData : Int = 1
    var bathroomsData : Int = 1
    var coolingData : Int = 1
    var wifiData : Int = 1
    var washingMachineData : Int = 1
    var heatingData : Int = 1
    var parkingData : Int = 1
    var wheelchairData : Int = 1
    //    --------------Accessibilities Variables Data-----------------
    var publicTransportData : Int = 1
    var storeData : Int = 1
    var shoppingData : Int = 1
    var restaurantData : Int = 1
    var swimmingData : Int = 1
    var hospitalData : Int = 1
    var fuelstationData : Int = 1
    var gymData : Int = 1
    //    --------------Tenants Variables Data-----------------
    var maleData: Int = 1
    var femaleData: Int = 1
    var noPreferenceData: Int = 1
    //    --------------HomeRules Variable Data-----------------
    var petFriendlyData: Int = 1
    var noSmokingData: Int = 1
    var quitePlaceData: Int = 1
    
    var getAddress: String = ""
    var latitude: Double = 0
    var longitude: Double = 0
    var rentValue: Int = 500
    private var datePickerInspection: UIDatePicker?
    private var datePickerAvailability: UIDatePicker?
    
    
    
    let db = Firestore.firestore()
    
    //    private var datePicker: UIDatePicker?
    var selectCapmus:String = ""
    
    var selectedMinStayLength:String = ""
    var selectedMaxStayLength:String = ""
    
    var selectedInspectionTime:String = ""
    var selectedPTVDistance:String = ""
    
    
    let options = ["Select Campus", "Albury-Wodonga", "Bundoora", "Bendigo", "Mildura", "Shepparton", "Sydney"]
    let ids = [0, 1, 2,3,4,5,6,7]
    
    let optionsStayLength = ["Select Minimum Stay", "No Minimum stay","1 month", "2 months", "3 months", "4 months", "5 months", "6 months", "7 months", "8 months", "9 months", "10 months", "11 months", "12 months+"]
    let idstayLength = [0, 1, 2,3,4,5,6,7,8,9,10,11,12,13]
    
    let optionsMaximumStayLength = ["Select Maximum Stay", "No Maximum Stay","1 month", "2 months", "3 months", "4 months", "5 months", "6 months", "7 months", "8 months", "9 months", "10 months", "11 months", "12 months+"]
    let idMaximumStayLength = [0, 1, 2,3,4,5,6,7,8,9,10,11,12,13]
    
    let optionsPTV = ["Within", "< 1 kms", "< 2 kms", "< 3 kms", "< 4 kms", "< 5 kms"]
    let idsPTV = [0, 1, 2,3,4,5,]
    
    let optionsSelectedFrom = ["From - To", "07:00 AM - 07:15 AM", "07:15 AM - 07:30 AM", "07:30 AM - 07:45 AM", "07:45 AM - 08:00 AM", "08:00 AM - 08:15 AM","08:15 AM - 08:30 AM", "08:30 AM - 08:45 AM", "08:45 AM - 09:00 AM", "09:00 AM - 09:15 AM", "09:15 AM - 09:30 AM", "09:30 AM - 09:45 AM", "09:45 AM - 10:00 AM", "10:00 AM - 10:15 AM", "10:15 AM - 10:30 AM", "10:30 AM - 10:45 AM", "10:45 AM - 11:00 AM", "11:00 AM - 11:15 AM", "11:15 AM - 11:30 AM", "11:30 AM - 11:45 AM", "11:45 AM - 12:00 PM", "12:00 PM - 12:15 PM", "12:15 PM - 12:30 PM", "12:30 PM - 12:45 PM", "12:45 PM - 01:00 PM", "01:00 PM - 01:15 PM",  "01:15 PM - 01:30 PM", "01:30 PM - 01:45 PM", "01:45 PM - 02:00 PM", "02:00 PM - 02:15 PM", "02:15 PM - 02:30 PM", "02:30 PM - 02:45 PM", "02:45 PM - 03:00 PM", "03:00 PM - 03:15 PM", "03:15 PM - 03:30 PM", "03:30 PM - 03:45 PM", "03:45 PM - 04:00 PM", "04:00 PM - 04:15 PM", "04:15 PM - 04:30 PM", "04:30 PM - 04:45 PM", "04:45 PM - 05:00 PM", "05:00 PM - 05:15 PM", "05:15 PM - 05:30 PM", "05:30 PM - 05:45 PM", "05:45 PM - 06:00 PM", "06:00 PM - 06:15 PM", "06:15 PM - 06:30 PM", "06:30 PM - 06:45 PM", "06:45 PM - 07:00 PM", "07:00 PM - 07:15 PM", "07:15 PM - 07:30 PM", "07:30 PM - 07:45 PM", "07:45 PM - 08:00 PM", "08:00 PM - 08:15 PM", "08:15 PM - 08:30 PM", "08:30 PM - 08:45 PM", "08:45 PM - 09:00 PM"]
    
//    let optionsSelectedFrom = ["From - To", "07:00 - 07:15", "07:15 - 07:30"]
    
    
    
    
    let idSelectedFrom = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swiftBillsIncluded.isOn = false
        activityIndicator.hidesWhenStopped = true
        saveButton.isEnabled = false
        propertyImage.isUserInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(chooseImage))
        propertyImage.addGestureRecognizer(gestureRecognizer)
        saveButton.layer.cornerRadius = 10
        searchAddress.layer.cornerRadius = 10
        propertyDetailTextView.delegate = self
        
        scrollView.delegate = self
        scrollToTop()
        
        addressMapMarker.isUserInteractionEnabled = false
        bedDetailsText.isHidden = true
        bedStepper.isHidden = true
        dropdownPTV.isHidden = true
        
        //------DROPDWN to select campus--
        dropDown.layer.cornerRadius = 10
        dropDown.setTitleColor(.label, for: .normal)
        dropDown.selectedRowColor = .lightGray
        dropDown.selectedRowTextColor = .label
        dropDown.borderWidth = 2
        dropDown.backgroundColor = .systemBackground
        //        dropDown.sele
        dropDown.rowHeight = 40
        
        dropDown.didSelect { (selectedText , index ,id) in
            
            
            
            
            self.selectCapmus = selectedText as String
            
            self.saveButton.isEnabled = true
            
            
        }
        
        //------DROPDWN to select MINIMUM stay lenght
        stayLength.layer.cornerRadius = 10
        //        stayLength.backgroundColor = .purple
        stayLength.setTitleColor(.label, for: .normal)
        stayLength.selectedRowColor = .lightGray
        stayLength.selectedRowTextColor = .label
        stayLength.borderWidth = 2
        stayLength.backgroundColor = .systemBackground
        stayLength.rowHeight = 40
        
        stayLength.didSelect { (selectedText , index ,id) in
            
            
            
            
            self.selectedMinStayLength = selectedText as String
            
            //            self.saveButton.isEnabled = true
            
            
        }
        
        
        stayLength.setOptions(options: optionsStayLength, optionIds: idstayLength, selectedIndex: 0)
        
        //------DROPDWN to select MAXIMUM stay lenght
        maximumStayLength.layer.cornerRadius = 10
        maximumStayLength.setTitleColor(.label, for: .normal)
        maximumStayLength.selectedRowColor = .lightGray
        maximumStayLength.selectedRowTextColor = .label
        maximumStayLength.borderWidth = 2
        maximumStayLength.backgroundColor = .systemBackground
        maximumStayLength.rowHeight = 40
        
        maximumStayLength.didSelect { (selectedText , index ,id) in
            
            
            
            
            self.selectedMaxStayLength = selectedText as String
            
            
            
        }
        
        
        maximumStayLength.setOptions(options: optionsMaximumStayLength, optionIds: idMaximumStayLength, selectedIndex: 0)
        
        //------DROPDWN for PTV
        dropdownPTV.layer.cornerRadius = 10
        //        stayLength.backgroundColor = .purple
        dropdownPTV.setTitleColor(.label, for: .normal)
        dropdownPTV.selectedRowColor = .lightGray
        dropdownPTV.selectedRowTextColor = .label
        dropdownPTV.borderWidth = 2
        dropdownPTV.backgroundColor = .systemBackground
        dropdownPTV.rowHeight = 40
        dropdownPTV.cellFont = UIFont.systemFont(ofSize: 10.0)
        dropdownPTV.didSelect { (selectedText , index ,id) in
            
            
            
            
            self.selectedPTVDistance = selectedText as String
            
            //            self.saveButton.isEnabled = true
            
            
        }
        
        
        dropdownPTV.setOptions(options: optionsPTV, optionIds: idsPTV, selectedIndex: 0)
        
        //------DROPDWN to select the inspection Time//
        
        dropDownFrom.layer.cornerRadius = 10
        dropDownFrom.setTitleColor(.label, for: .normal)
        dropDownFrom.selectedRowColor = .lightGray
        dropDownFrom.selectedRowTextColor = .label
        dropDownFrom.borderWidth = 2
        dropDownFrom.backgroundColor = .systemBackground
        dropDownFrom.rowHeight = 40
        
        dropDownFrom.didSelect { (selectedText , index ,id) in
            
            
            self.selectedInspectionTime = selectedText as String
            
            
        }
        
        dropDownFrom.setOptions(options: optionsSelectedFrom, optionIds: idSelectedFrom, selectedIndex: 0)
        
        
//        let theHour = self.selectedInspectionTime.getMyDate().hour
//
//
        
        
        //DATEPICKER INSPECTION//
        
        datePickerInspection = UIDatePicker()
        datePickerAvailability = UIDatePicker()
        datePickerInspection?.datePickerMode = .date
        datePickerAvailability?.datePickerMode = .date
        //        datePickerInspection?.addTarget(self, action: #selector(dateChanged(datePicker: datePickerInspection!)), for: .valueChanged)
        datePickerInspection?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: UIControl.Event.valueChanged)
        datePickerAvailability?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: UIControl.Event.valueChanged)
        
        inspectionFromTextField.inputView = datePickerInspection
        availableFromTextField.inputView = datePickerAvailability
        
        //ADDING TOOLBAR
        
        
        let availabilityToolbar = UIToolbar()
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor (red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        availabilityToolbar.barStyle = .default
        availabilityToolbar.isTranslucent = true
        availabilityToolbar.tintColor = UIColor (red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        availabilityToolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(PostPropertyViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(PostPropertyViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        inspectionFromTextField.inputAccessoryView = toolBar
        
        
        
        let doneButtonAvailability = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(PostPropertyViewController.doneClickAvailability))
        let spaceButtonAvailability = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButtonAvailability = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(PostPropertyViewController.cancelClickAvailability))
        
        availabilityToolbar.setItems([cancelButtonAvailability, spaceButtonAvailability, doneButtonAvailability], animated: false)
        availabilityToolbar.isUserInteractionEnabled = true
        availableFromTextField.inputAccessoryView = availabilityToolbar
        
        
        
        
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        userAuthentication()
        addressMapMarker.isUserInteractionEnabled = false
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        scrollToTop()
        initializePage()
        addressMapMarker.isUserInteractionEnabled = false
        //        userAuthentication()
    }
    
    //    --------------- Automatic scorll to top of the page -----------------
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -self.scrollView.contentInset.top)
        self.scrollView.setContentOffset(desiredOffset, animated: true)
    }
    
    
    
    func userAuthentication(){
        //        if Auth.auth().currentUser != nil {
        //            let alert = UIAlertController(title: "User Allowed to post", message: "", preferredStyle: UIAlertController.Style.alert)
        //           let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        //            alert.addAction(okButton)
        //            self.present(alert, animated: true, completion: nil)
        //
        //        } else
        if Auth.auth().currentUser == nil  {
            performSegue(withIdentifier: "ToWelcomePage", sender: self)
            let alert = UIAlertController(title: "Please Login to post an ad", message: "", preferredStyle: UIAlertController.Style.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
            //            User Not logged in
        }
    }
    
    //    ---------------- Pressing return key goes to the next field --------------
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == addressTextField {
            //            rentTextField.becomeFirstResponder()
            
            textField.resignFirstResponder()
            
            
        }
        
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textView.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        let characterCount = propertyDetailTextView.text.count
        self.characterLabel.text = "\(characterCount)/300"
        // make sure the result is under 16 characters
        return updatedText.count <= 300
        
    }
    
    
    
    //    --------------- Choosing image from Gallery -------------
    
    @objc func chooseImage(){
        
        let pickerContorller = UIImagePickerController()
        pickerContorller.delegate = self
        pickerContorller.sourceType = .photoLibrary
        present(pickerContorller, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        propertyImage.image = info[.originalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //    --------------- Function of DatePicker(AVAIABILITY) -------------
    
    
    
    @objc func doneClick(datePicker: UIDatePicker) {
        dateChanged(datePicker: datePickerInspection!)
        
        datePickerInspection!.resignFirstResponder()
        
        self.view.endEditing(true)
    }
    @objc func cancelClick(datePicker: UIDatePicker) {
        //Cancel button dismiss datepicker dialog
        inspectionFromTextField.text = ""
        datePickerInspection!.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func doneClickAvailability(datePicker: UIDatePicker) {
        dateChanged(datePicker: datePickerAvailability!)
        
        datePickerAvailability!.resignFirstResponder()
        
        self.view.endEditing(true)
    }
    @objc func cancelClickAvailability(datePicker: UIDatePicker) {
        //Cancel button dismiss datepicker dialog
        availableFromTextField.text = ""
        datePickerAvailability!.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker ) {
        if datePicker == datePickerInspection{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            //        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
            inspectionFromTextField.text = dateFormatter.string(from: datePicker.date);
        } else if datePicker == datePickerAvailability{
            let dateFormatter = DateFormatter()
            
            //----- save the date picker's date into availableDate variable
            availableDate = datePicker.date
            //--------------------------------------------------------------
            
            dateFormatter.dateFormat = "dd MMM yyyy"
            availableFromTextField.text = dateFormatter.string(from: datePicker.date);
        }
        
        
        
    }
    
    
    
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if Auth.auth().currentUser == nil  {
            performSegue(withIdentifier: "ToWelcomePage", sender: self)
        }
        else{
            activityIndicator.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
            let storage = Storage.storage()
            let storageReference = storage.reference()
            
            let mediaFolder = storageReference.child("media")
            
            //Function of Bills Swift//
            //        billSwtichStatusCheck()
            
            if let data = propertyImage.image?.jpegData(compressionQuality: 0.5) {
                
                let uuid = UUID().uuidString
                
                let imageReference = mediaFolder.child("\(uuid).jpg")
                imageReference.putData(data, metadata: nil) { (metadata, error) in
                    if error != nil {
                        alertNotification(Title: "Error!", Message: error?.localizedDescription ?? "Error")
                    } else {
                        
                        imageReference.downloadURL { (url, error) in
                            
                            if error == nil {
                                
                                let imageUrl = url?.absoluteString
                                if let address = self.addressTextField.text,
                                    
                                    
                                    let imageData = self.propertyImage.image?.jpegData(compressionQuality: 0.5)
                                    //                                let imageData: NSData = propertyImage.image!.pngData() as NSData?
                                {
                                    let propertyIDData = UUID()
                                    let propertyID = propertyIDData.uuidString
                                    let campus = self.selectCapmus
                                    
                                    print(campus)
                                    self.db.collection("Property").addDocument(data: ["ID" : propertyID,
                                                                                      "Address" : address,
                                                                                      "Rent" : self.rentValue,
                                                                                      "PropertyImage" : imageUrl,
                                                                                      "DateField": Date().timeIntervalSince1970,
                                                                                      "Campus" : campus,
                                                                                      "Apartment" : self.apartmentData,
                                                                                      "StudioApartment" : self.studioAptData,
                                                                                      "SharedRoom" : self.sharedRoomData,
                                                                                      "PrivateRoom" : self.privateRoomData,
                                                                                      "Beds" : self.bedsData,
                                                                                      "StudyTabel" : self.studyTableData,
                                                                                      "Wardrobe" : self.wardrobeData,
                                                                                      "KitchenAmenities" : self.kitchenAmenitiesData,
                                                                                      "Rooms" : self.roomsData,
                                                                                      "Bathrooms" : self.bathroomsData,
                                                                                      "Cooling" : self.coolingData,
                                                                                      "Wifi" : self.wifiData,
                                                                                      "WashingMachine" : self.washingMachineData,
                                                                                      "Heating" : self.heatingData,
                                                                                      "Parking" : self.parkingData,
                                                                                      "WheelChair" : self.wheelchairData,
                                                                                      "PublicTransport" : self.publicTransportData,
                                                                                      "Store" : self.storeData,
                                                                                      "Shopping" : self.shoppingData,
                                                                                      "Restaurant" : self.restaurantData,
                                                                                      "Swimming" : self.swimmingData,
                                                                                      "Hospital" : self.hospitalData,
                                                                                      "FuelStation" : self.fuelstationData,
                                                                                      "Gym" : self.gymData,
                                                                                      "Male" : self.maleData,
                                                                                      "Female" : self.femaleData,
                                                                                      "NoPreference" : self.noPreferenceData,
                                                                                      "PetFriendly" : self.petFriendlyData,
                                                                                      "NoSmoking" : self.noSmokingData,
                                                                                      "QuitePlace" : self.quitePlaceData,
                                                                                      "Latitude" : self.latitude,
                                                                                      "Longitude": self.longitude,
                                                                                      "Bill" :
                                                                                        self.billData,
                                                                                      "AvailableFrom" : self.availableFromTextField.text,
                                                                                      
                                                                                      //------- a new date related piece of data sent to firebase
                                        "AvailableFromDate" : self.availableDate,
                                        
                                                                     //-------------------------------
                                                                                      "InspectionDate" : self.inspectionFromTextField.text,
                                                                                      "InspectionTime" : self.selectedInspectionTime,
                                                                                      "MaxStay" : self.selectedMaxStayLength,
                                                                                      "MinStay" : self.selectedMinStayLength,
                                                                                      "Title" : self.propertyTitleTextField.text,
                                                                                      "Description" : self.propertyDetailTextView.text,
                                                                                      "PostedBy":
                                                                                        Auth.auth().currentUser?.email as! String])
                                    { (error) in
                                        if let e = error{
                                            print("There was an isssue saving the property, \(e)")
                                            alertNotification(Title: "Not Posted!", Message: "Not saved to the database")
                                            
                                        }
                                        else{
                                            print("Successfully saved the Property Details")
                                            
                                            //                    self.dismiss(animated: true, completion: nil)
                                            
                                            self.initializePage()
                                            
                                            
                                            
                                            self.performSegue(withIdentifier: "propertyPostedToPostedByView", sender: nil)
                                            self.activityIndicator.stopAnimating()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                            //                                         alertNotification(Title: "Posted!", Message: "Your Property has been successfully posted in Latrobe Accomodation")
                                            
                                        }
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        
        func alertNotification(Title : String, Message : String){
            
            let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "propertyPostedToPostedByView" {
            let destinationVC = segue.destination as! PostedByViewController
            //            destinationVC.selectedCapmus = selectCapmus
            
        }
    }
    
    //    func billSwtichStatusCheck () {
    //        if swiftBillsIncluded.isOn == true {
    //            billData = 0
    //        } else {
    //            swiftBillsIncluded.isOn = false
    //            billData = 1
    //        }
    //    }
    //    func customizeButtonG1(searchAddress: UIButton) {
    //           // change UIbutton propertie
    //                 let c1GreenColor = (UIColor(red: -0.108958, green: 0.714926, blue: 0.758113, alpha: 1.0))
    //                 let c2GreenColor = (UIColor(red: 0.108958, green: 0.714926, blue: 0.758113, alpha: 1.0))
    ////                 searchAddress.backgroundColor = UIColor.red
    //                 searchAddress.layer.cornerRadius = 7
    //                 searchAddress.layer.borderWidth = 0.8
    //                 searchAddress.layer.borderColor = c1GreenColor.cgColor
    //
    //                 searchAddress.layer.shadowColor = c2GreenColor.cgColor
    //                 searchAddress.layer.shadowOpacity = 0.8
    //                 searchAddress.layer.shadowRadius = 12
    ////                 searchAddress.layer.shadowOffset = CGSize(width: 1, height: 1)
    //
    ////                 searchAddress.setImage(UIImage(named:"3d-glass-refresh-32X32.png"), for: .normal)
    ////                 searchAddress.imageEdgeInsets = UIEdgeInsets(top: 6,left: 100,bottom: 6,right: 14)
    ////                 searchAddress.titleEdgeInsets = UIEdgeInsets(top: 0,left: -30,bottom: 0,right: 34)
    //
    //             }
    //
    //         }
    
    
    
    
    
    
    
    
    //    --------------Property Type Buttons Pressed-----------------
    
    @IBAction func swiftBillsButton(_ sender: Any) {
        if swiftBillsIncluded.isOn == true {
            billData = 0
            print("--------------------- BIll DATA\(billData)")
        } else {
            swiftBillsIncluded.isOn = false
            billData = 1
            print("--------------------- BIll DATA\(billData)")
        }
    }
    
    
    @IBAction func apartmentButtonTapped(_ sender: Any) {
        
        apartmentFlipped.toggle()
        
        if apartmentFlipped{
            apartmentButton.tintColor = UIColor.label
            apartmentData = 0
            
        }else{
            apartmentButton.tintColor = UIColor.lightGray
            apartmentData = 1
            
        }
    }
    
    @IBAction func studioAptButtonTapped(_ sender: Any) {
        
        studioAptFlipped.toggle()
        
        if studioAptFlipped{
            studioAptButton.tintColor = UIColor.label
            studioAptData = 0
            
        }else{
            studioAptButton.tintColor = UIColor.lightGray
            studioAptData = 1
            
            
        }
    }
    
    @IBAction func sharedRoomButtonTapped(_ sender: Any) {
        
        sharedRoomFlipped.toggle()
        
        if sharedRoomFlipped{
            sharedRoomButton.tintColor = UIColor.label
            sharedRoomData = 0
            
        }else{
            sharedRoomButton.tintColor = UIColor.lightGray
            sharedRoomData = 1
            
        }
    }
    
    @IBAction func privateRoomButtonTapped(_ sender: Any) {
        
        privateRoomFlipped.toggle()
        
        if privateRoomFlipped{
            privateRoomButton.tintColor = UIColor.label
            privateRoomData = 0
            
        }else{
            privateRoomButton.tintColor = UIColor.lightGray
            privateRoomData = 1
            
        }
    }
    
    
    
    
    //    --------------Furnished Buttons Pressed-----------------
    
    
    
    
    @IBAction func bedsButtonTapped(_ sender: Any) {
        
        bedsFlipped.toggle()
        
        if bedsFlipped{
            bedsButton.tintColor = UIColor.label
            bedsData = 0
            bedDetailsText.isHidden = false
            bedStepper.isHidden = false
            
        }else{
            bedsButton.tintColor = UIColor.lightGray
            bedsData = 1
            bedDetailsText.isHidden = true
            bedStepper.isHidden = true
            
        }
    }
    @IBAction func studyTableButtonTapped(_ sender: Any) {
        
        studyTableFlipped.toggle()
        
        if studyTableFlipped{
            studyTableButton.tintColor = UIColor.label
            studyTableData = 0
            
        }else{
            studyTableButton.tintColor = UIColor.lightGray
            studyTableData = 1
            
        }
        
    }
    
    @IBAction func wardrobeButtonTapped(_ sender: Any) {
        
        wardrobeFlipped.toggle()
        
        if wardrobeFlipped{
            wardrobeButton.tintColor = UIColor.label
            wardrobeData = 0
            
        }else{
            wardrobeButton.tintColor = UIColor.lightGray
            wardrobeData = 1
            
        }
        
    }
    
    @IBAction func kitchenAmenitiesButtonTapped(_ sender: Any) {
        
        kitchenAmenitiesFlipped.toggle()
        
        if kitchenAmenitiesFlipped{
            kitchenAmenitiesButton.tintColor = UIColor.label
            kitchenAmenitiesData = 0
            
        }else{
            kitchenAmenitiesButton.tintColor = UIColor.lightGray
            kitchenAmenitiesData = 1
        }
    }
    
    
    //    --------------Features Buttons Pressed-----------------
    
    @IBAction func roomsButtonTapped(_ sender: Any) {
        
        roomsFlipped.toggle()
        
        if roomsFlipped{
            roomsButton.tintColor = UIColor.label
            roomsData = 0
            
        }else{
            roomsButton.tintColor = UIColor.lightGray
            roomsData = 1
            
        }
    }
    
    @IBAction func bathroomsButtonTapped(_ sender: Any) {
        
        bathroomsFlipped.toggle()
        
        if bathroomsFlipped{
            bathroomsButton.tintColor = UIColor.label
            bathroomsData = 0
            
        }else{
            bathroomsButton.tintColor = UIColor.lightGray
            bathroomsData = 1
            
        }
    }
    
    @IBAction func coolingButtonTapped(_ sender: Any) {
        
        coolingFlipped.toggle()
        
        if coolingFlipped{
            coolingButton.tintColor = UIColor.label
            coolingData = 0
            
        }else{
            coolingButton.tintColor = UIColor.lightGray
            coolingData = 1
            
        }
    }
    
    @IBAction func wifiButtonTapped(_ sender: Any) {
        
        wifiFlipped.toggle()
        
        if wifiFlipped{
            wifiButton.tintColor = UIColor.label
            wifiData = 0
            
        }else{
            wifiButton.tintColor = UIColor.lightGray
            wifiData = 1
            
        }
    }
    
    @IBAction func washingMachineButtonTapped(_ sender: Any) {
        
        washingMachineFlipped.toggle()
        
        if washingMachineFlipped{
            washingMachineButton.tintColor = UIColor.label
            washingMachineData = 0
            
        }else{
            washingMachineButton.tintColor = UIColor.lightGray
            washingMachineData = 1
            
        }
    }
    
    @IBAction func heatingButtonTapped(_ sender: Any) {
        
        heatingFlipped.toggle()
        
        if heatingFlipped{
            heatingButton.tintColor = UIColor.label
            heatingData = 0
            
        }else{
            heatingButton.tintColor = UIColor.lightGray
            heatingData = 1
            
        }
    }
    
    @IBAction func parkingButtonTapped(_ sender: Any) {
        
        parkingFlipped.toggle()
        
        if parkingFlipped{
            parkingButton.tintColor = UIColor.label
            parkingData = 0
            
        }else{
            parkingButton.tintColor = UIColor.lightGray
            parkingData = 1
            
        }
    }
    
    @IBAction func wheelchairButtonTapped(_ sender: Any) {
        
        wheelchairFlipped.toggle()
        
        if wheelchairFlipped{
            wheelchairButton.tintColor = UIColor.label
            wheelchairData = 0
            
        }else{
            wheelchairButton.tintColor = UIColor.lightGray
            wheelchairData = 1
            
        }
        
        
    }
    
    
    //    --------------Accessibilities Buttons Pressed-----------------
    
    @IBAction func publicTransportButtonTapped(_ sender: Any) {
        
        publicTransportFlipped.toggle()
        
        if publicTransportFlipped{
            publicTransportButton.tintColor = UIColor.label
            publicTransportData = 0
            dropdownPTV.isHidden = false
            
        }else{
            publicTransportButton.tintColor = UIColor.lightGray
            publicTransportData = 1
            dropdownPTV.isHidden = true
            
        }
    }
    
    @IBAction func storeButtonTapped(_ sender: Any) {
        
        storeFlipped.toggle()
        
        if storeFlipped{
            storeButton.tintColor = UIColor.label
            storeData = 0
            
        }else{
            storeButton.tintColor = UIColor.lightGray
            storeData = 1
            
        }
    }
    
    @IBAction func shoppingButtonTapped(_ sender: Any) {
        
        shoppingFlipped.toggle()
        
        if shoppingFlipped{
            shoppingButton.tintColor = UIColor.label
            shoppingData = 0
            
        }else{
            shoppingButton.tintColor = UIColor.lightGray
            shoppingData = 1
            
        }
    }
    
    @IBAction func restaurantButtonTapped(_ sender: Any) {
        
        restaurantFlipped.toggle()
        
        if restaurantFlipped{
            restaurantButton.tintColor = UIColor.label
            restaurantData = 0
            
        }else{
            restaurantButton.tintColor = UIColor.lightGray
            restaurantData = 1
            
        }
    }
    
    
    @IBAction func swimmingButtonTapped(_ sender: Any) {
        
        swimmingFlipped.toggle()
        
        if swimmingFlipped{
            swimmingButton.tintColor = UIColor.label
            swimmingData = 0
            
        }else{
            swimmingButton.tintColor = UIColor.lightGray
            swimmingData = 1
            
        }
    }
    
    
    @IBAction func hospitalButtonTapped(_ sender: Any) {
        
        hospitalFlipped.toggle()
        
        if hospitalFlipped{
            hospitalButton.tintColor = UIColor.label
            hospitalData = 0
            
            
        }else{
            hospitalButton.tintColor = UIColor.lightGray
            hospitalData = 1
            
        }
    }
    
    
    @IBAction func fuelButtonTapped(_ sender: Any) {
        
        fuelstationFlipped.toggle()
        
        if fuelstationFlipped{
            fuelStationButton.tintColor = UIColor.label
            fuelstationData = 0
            
        }else{
            fuelStationButton.tintColor = UIColor.lightGray
            fuelstationData = 1
            
        }
    }
    
    @IBAction func gymButtonTapped(_ sender: Any) {
        
        gymFlipped.toggle()
        
        if gymFlipped{
            gymButton.tintColor = UIColor.label
            gymData = 0
            
        }else{
            gymButton.tintColor = UIColor.lightGray
            gymData = 1
            
        }
    }
    
    
    
    //    --------------Tenants Buttons Pressed-----------------
    @IBAction func maleButtonTapped(_ sender: Any) {
        
        maleFlipped.toggle()
        
        if maleFlipped{
            maleButton.tintColor = UIColor.label
            maleData = 0
            
        }else{
            maleButton.tintColor = UIColor.lightGray
            maleData = 1
            
        }
    }
    
    @IBAction func femaleButtonTapped(_ sender: Any) {
        
        femaleFlipped.toggle()
        
        if femaleFlipped{
            femaleButton.tintColor = UIColor.label
            femaleData = 0
            
        }else{
            femaleButton.tintColor = UIColor.lightGray
            femaleData = 1
            
        }
    }
    
    @IBAction func noPreferenceButtonTapped(_ sender: Any) {
        
        noPreferenceFlipped.toggle()
        
        if noPreferenceFlipped{
            noPreferenceButton.tintColor = UIColor.label
            noPreferenceData = 0
            
        }else{
            noPreferenceButton.tintColor = UIColor.lightGray
            noPreferenceData = 1
            
            
        }
    }
    
    
    //    --------------HomeRules Buttons Pressed-----------------
    @IBAction func petFriendlyButtonTapped(_ sender: Any) {
        
        
        petFriendlyFlipped.toggle()
        
        if petFriendlyFlipped{
            petFriendlyButton.tintColor = UIColor.label
            petFriendlyData = 0
            
            
        }else{
            petFriendlyButton.tintColor = UIColor.lightGray
            petFriendlyData = 1
            
        }
    }
    
    
    @IBAction func noSmokingButtonTapped(_ sender: Any) {
        
        noSmokingFlipped.toggle()
        
        if noSmokingFlipped{
            noSmokingButton.tintColor = UIColor.label
            noSmokingData = 0
            
        }else{
            noSmokingButton.tintColor = UIColor.lightGray
            noSmokingData = 1
            
        }
    }
    
    @IBAction func quitePlaceButtonTapped(_ sender: Any) {
        
        quitePlaceFlipped.toggle()
        
        if quitePlaceFlipped{
            quitePlaceButton.tintColor = UIColor.label
            quitePlaceData = 0
            
        }else{
            quitePlaceButton.tintColor = UIColor.lightGray
            quitePlaceData = 1
            
        }
    }
    
    @IBAction func rentSliderChanged(_ sender: UISlider) {
        rentValue = Int(sender.value)
        rentLabel.text = "$\(rentValue)"
        
    }
    
    
    
    func gotoPlaces() {
        
        
        
        //         txtSearch?.resignFirstResponder()
        
        let asController = GMSAutocompleteViewController()
        asController.delegate = self
        present(asController, animated: true, completion: nil)
        
        
        /*let filter = GMSAutocompleteFilter()
         filter.type = .address
         filter.country = "AUS"
         asController.autocompleteFilter = filter*/
    }
    
    
    
    
    @IBAction func searchAddressTapped(_ sender: Any) {
        gotoPlaces()
        
        //        searchAddress.isHidden = true
        //        refreshAddress.isHidden = false
        
    }
    
    
    @IBAction func refreshButton(_ sender: Any) {
        gotoPlaces()
        
    }
    
    
    
    
    func initializePage(){
        searchAddress.isHidden = false
        saveButton.isEnabled = false
        self.addressTextField.text = ""
        self.rentSlider.value = 500
        self.rentLabel.text = "$500"
        self.inspectionFromTextField.text = ""
        self.availableFromTextField.text = ""
        self.propertyDetailTextView.text = ""
        self.propertyTitleTextField.text = ""
        self.bedStepper.value = 0
        self.bedDetailsText.text = ""
        self.propertyImage.image = #imageLiteral(resourceName: "AddImage2")
        self.addressMapMarker.clear()
        dropDown.setOptions(options: options, optionIds: ids, selectedIndex: 0)
        stayLength.setOptions(options: optionsStayLength, optionIds: idstayLength, selectedIndex: 0)
        dropDownFrom.setOptions(options: optionsSelectedFrom, optionIds: idSelectedFrom, selectedIndex: 0)
        dropdownPTV.setOptions(options: optionsPTV, optionIds: idsPTV, selectedIndex: 0)
        
        swiftBillsIncluded.isOn = false
        
        
        
        
        bedDetailsText.isHidden = true
        bedStepper.isHidden = true
        dropdownPTV.isHidden = true
        refreshAddress.isHidden = true
        //    --------------Property Type variables-----------------
        apartmentFlipped = false
        studioAptFlipped = false
        sharedRoomFlipped = false
        privateRoomFlipped = false
        //    --------------Furnished variables-----------------
        bedsFlipped  = false
        studyTableFlipped  = false
        wardrobeFlipped  = false
        kitchenAmenitiesFlipped  = false
        //    --------------Features variables-----------------
        roomsFlipped  = false
        bathroomsFlipped  = false
        coolingFlipped  = false
        wifiFlipped  = false
        washingMachineFlipped  = false
        heatingFlipped  = false
        parkingFlipped  = false
        wheelchairFlipped  = false
        //    --------------Accessibilities variables-----------------
        publicTransportFlipped  = false
        storeFlipped  = false
        shoppingFlipped  = false
        restaurantFlipped  = false
        swimmingFlipped  = false
        hospitalFlipped  = false
        fuelstationFlipped  = false
        gymFlipped  = false
        //    --------------Tenants variables-----------------
        maleFlipped = false
        femaleFlipped = false
        noPreferenceFlipped = false
        //    --------------HomeRules variables-----------------
        petFriendlyFlipped = false
        noSmokingFlipped = false
        quitePlaceFlipped = false
        
        
        
        apartmentButton.tintColor = UIColor.lightGray
        studioAptButton.tintColor = UIColor.lightGray
        sharedRoomButton.tintColor = UIColor.lightGray
        privateRoomButton.tintColor = UIColor.lightGray
        
        bedsButton.tintColor = UIColor.lightGray
        studyTableButton.tintColor = UIColor.lightGray
        wardrobeButton.tintColor = UIColor.lightGray
        kitchenAmenitiesButton.tintColor = UIColor.lightGray
        
        roomsButton.tintColor = UIColor.lightGray
        bathroomsButton.tintColor = UIColor.lightGray
        coolingButton.tintColor = UIColor.lightGray
        wifiButton.tintColor = UIColor.lightGray
        washingMachineButton.tintColor = UIColor.lightGray
        heatingButton.tintColor = UIColor.lightGray
        parkingButton.tintColor = UIColor.lightGray
        wheelchairButton.tintColor = UIColor.lightGray
        
        publicTransportButton.tintColor = UIColor.lightGray
        storeButton.tintColor = UIColor.lightGray
        shoppingButton.tintColor = UIColor.lightGray
        restaurantButton.tintColor = UIColor.lightGray
        swimmingButton.tintColor = UIColor.lightGray
        hospitalButton.tintColor = UIColor.lightGray
        fuelStationButton.tintColor = UIColor.lightGray
        gymButton.tintColor = UIColor.lightGray
        
        maleButton.tintColor = UIColor.lightGray
        femaleButton.tintColor = UIColor.lightGray
        noPreferenceButton.tintColor = UIColor.lightGray
        
        petFriendlyButton.tintColor = UIColor.lightGray
        noSmokingButton.tintColor = UIColor.lightGray
        quitePlaceButton.tintColor = UIColor.lightGray
        billData = 1
        //    --------------Property Type variablesData-----------------
        apartmentData = 1
        studioAptData = 1
        sharedRoomData = 1
        privateRoomData = 1
        //    --------------Furnished variablesData-----------------
        bedsData  = 1
        studyTableData  = 1
        wardrobeData  = 1
        kitchenAmenitiesData  = 1
        //    --------------Features variablesData-----------------
        roomsData  = 1
        bathroomsData  = 1
        coolingData  = 1
        wifiData  = 1
        washingMachineData  = 1
        heatingData  = 1
        parkingData  = 1
        wheelchairData  = 1
        //    --------------Accessibilities variablesData-----------------
        publicTransportData  = 1
        storeData  = 1
        shoppingData  = 1
        restaurantData  = 1
        swimmingData  = 1
        hospitalData  = 1
        fuelstationData  = 1
        gymData  = 1
        //    --------------Tenants variablesData-----------------
        maleData = 1
        femaleData = 1
        noPreferenceData = 1
        //    --------------HomeRules Buttons-----------------
        petFriendlyData = 1
        noSmokingData = 1
        quitePlaceData = 1
        
        
        
    }
    
}

extension PostPropertyViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        //lblLocation.text = "latitude = \(locValue.latitude), longitude = \(locValue.longitude)"
    }
}



extension PostPropertyViewController: GMSAutocompleteViewControllerDelegate {
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //        print("Place name: \(String(describing: place.name))")
        dismiss(animated: true, completion: nil)
        
        self.addressMapMarker.clear()
        
        
        
        let cord2D = CLLocationCoordinate2D(latitude: (place.coordinate.latitude), longitude: (place.coordinate.longitude))
        //        let cord3D = CLLocationCoordinate2D(latitude: 0,longitude: 0)
        
        let marker = GMSMarker()
        marker.position =  cord2D
        //        marker.position =  cord3D
        marker.title = place.name
        marker.snippet = place.formattedAddress
        marker.appearAnimation = .pop
        getAddress = String(place.formattedAddress!)
        addressTextField.text = getAddress
        latitude = place.coordinate.latitude
        longitude = place.coordinate.longitude
        //let markerImage = UIImage(named: "icon_offer_pickup")!
        //let markerView = UIImageView(image: markerImage)
        //marker.iconView = markerView
        marker.map = self.addressMapMarker
        
        if addressTextField.text != ""{
            searchAddress.isHidden = true
            refreshAddress.isHidden = false
        }
        
        self.addressMapMarker.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
}

//extension String {
//    func getMyDate() -> DateComponents {
//
//        var dateComp = DateComponents()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:MM - HH:MM"
//
//
//
//        if let date = dateFormatter.date(from: self) {
//        dateComp = Calendar.current.dateComponents([.hour, .minute], from: date)
//
//        }
//        return dateComp


