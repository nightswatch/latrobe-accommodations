//
//  ResultsPropertyDetails.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 22/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import Foundation


class postedbyPropertyDetails: NSObject {
    
    var address : String
    var rent : Double
    var imageData : String
    var campus : String
    var id : String
 
    var dateField : Double
    

    
    init(propertyAddress: String?, propertyRent:Double?, propertyImage:String?, propertyCampus:String?, propertyID: String?, propertydateField : Double?) {
        
        address = propertyAddress!
        rent = propertyRent!
        imageData = propertyImage!
        campus = propertyCampus!
        id = propertyID!
        dateField  = propertydateField!

        
    }
    
}
