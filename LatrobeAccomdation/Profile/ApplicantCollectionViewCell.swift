//
//  ApplicantCollectionViewCell.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 30/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit

class ApplicantCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var firstName: UILabel!
    
    @IBOutlet weak var lastName: UILabel!
    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImage.layer.borderWidth = 1.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.clipsToBounds = true
        
    }

}
