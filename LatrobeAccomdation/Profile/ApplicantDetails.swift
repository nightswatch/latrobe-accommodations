//
//  ApplicantDetails.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 31/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import Foundation


class applicantDetails: NSObject {
    
    var firstName : String
    var lastName : String
    var imageData : String
    
    init(applicantFirstName: String?, applicantLastName:String?, UserImage:String?){
        
        
        firstName = applicantFirstName!
        lastName = applicantLastName!
        imageData = UserImage!
        
    }
    
}
