//
//  ApplicantViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 30/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase

class ApplicantViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    @IBOutlet weak var applicantCollectionView: UICollectionView!
    
    var propertyID : String = ""
    
    var applicantArray = [applicantDetails]()
    
    
    let db = Firestore.firestore()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadProperties()
        
    }
    
    
    func loadProperties(){
        
        db.collection("Applications").whereField("PropertyID", isEqualTo: propertyID).addSnapshotListener { (querySnapshot, error) in
            
            if let e = error{
                print("There was an error in retrieving Application \(e)")
                self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
            }else {
                if let snapshotDocument = querySnapshot?.documents{
                    for doc in snapshotDocument{
                        let data = doc.data()
                        if let userID = data["TenantID"] as? String{
                            
                            self.db.collection("User").whereField("ID", isEqualTo: userID).addSnapshotListener { (querySnapshot, error) in
                                
                                if let e = error{
                                    print("There was an error in retrieving User Information \(e)")
                                    self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                                }else {
                                    if let snapshotDocument = querySnapshot?.documents{
                                        for doc in snapshotDocument{
                                            let data = doc.data()
                                            
                                            if let firstName = data["FirstName"] as? String,
                                                let lastName = data["LastName"] as? String,
                                                let profileImageData = data["ProfilePicture"]as? String{
                                                
                                                let userdetails = applicantDetails(applicantFirstName: firstName, applicantLastName: lastName, UserImage: profileImageData)
                                                
                                                self.applicantArray.append(userdetails)
                                                
                                            }
                                            DispatchQueue.main.async {
                                                self.applicantCollectionView.reloadData()
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return applicantArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "applicantCell", for: indexPath)as! ApplicantCollectionViewCell
        cell.firstName?.text = applicantArray[indexPath.row].firstName
        cell.lastName?.text = applicantArray[indexPath.row].lastName
        cell.profileImage?.sd_setImage(with: URL(string: self.applicantArray[indexPath.row].imageData))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    
    func alertNotification(Title : String, Message : String){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
