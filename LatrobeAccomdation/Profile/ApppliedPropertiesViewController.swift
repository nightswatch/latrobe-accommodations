//
//  ApppliedPropertiesViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 24/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase

class ApppliedPropertiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

     @IBOutlet weak var appliedToTabelview: UITableView!
        @IBOutlet weak var propertyCountLabel: UILabel!
        
        var resultProperties = [postedbyPropertyDetails]()
        var propertyID:String = ""
        let db = Firestore.firestore()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            appliedToTabelview.delegate = self
            appliedToTabelview.dataSource = self
            loadProperties()
            print(Auth.auth().currentUser?.email! as! String)
            // Do any additional setup after loading the view.
        }
    
    override func viewWillAppear(_ animated: Bool) {
        loadProperties()
    }
        
        
        func loadProperties(){
            
            
            db.collection("Applications")
                .order(by: "DateField", descending: true)
                .whereField("TenantID", isEqualTo: Auth.auth().currentUser?.email! as! String)
                .addSnapshotListener
                {
                    (querySnapshot, error) in
                    self.resultProperties = []
                    if let e = error{
                        print("There was an error in retrieving data \(e)")
                        self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                    }else {
                        if let snapshotDocument = querySnapshot?.documents{
                            for doc in snapshotDocument{
                                let data = doc.data()
                                if let propertyAddress = data["Address"] as? String,
                                    let propertyRent = data["Rent"] as? Double,
                                    let propertyID = data["PropertyID"] as? String,
                                    let propertyImageData = data["ImageData"]as? String,
                                    let propertyCampus = data["Campus"]as? String,
                                    let dateField = data["DateField"] as? Double{
                                    
                                    let propertyFirebase = postedbyPropertyDetails(propertyAddress: propertyAddress, propertyRent: propertyRent, propertyImage: propertyImageData, propertyCampus:propertyCampus , propertyID: propertyID, propertydateField: dateField)
                                    
                                    self.resultProperties.append(propertyFirebase)
                                    
                                    DispatchQueue.main.async {
                                        self.appliedToTabelview.reloadData()
                                        self.propertyCountLabel.text = "Properties You have Applied to : \(self.resultProperties.count)"
                                    }
                                    
                                }
                            }
                            
                        }
                    }
            }
        }
        
        
        
        
        
        //----------------------------NUMBER OF ROWS IN THE TABELVIEW--------------------------------
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if resultProperties.count != 0 {
                return (resultProperties.count)
            }else{
                return 0
            }
            
        }
        
        //------------------------WHICH CELL SHOULD TAKE WHICH ROW-------------------------
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "propertListCell", for: indexPath)as! PropertyDetailsSecondaryViewCell
            
            cell.propertyAddress?.text = resultProperties[indexPath.row].address
            cell.propertyRent?.text = "$\(String(Int(resultProperties[indexPath.row].rent)))"
            cell.prepertyImage?.sd_setImage(with: URL(string: self.resultProperties[indexPath.row].imageData))
            
            return cell
            
        }
        
        
        
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 375
        }
        
        //        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        //            propertyID = resultProperties[indexPath.row].id
        //            //        print("UUID = \(propertyID)")
        //            performSegue(withIdentifier: "propertyPostedToResultsView", sender: nil)
        //        }
        
        //        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //            if segue.identifier == "" {
        //                let destinationVC = segue.destination as! PropertyDetailViewController
        //                destinationVC.propertyID = propertyID
        //
        //            }
        //        }
        
        
        
        
        func alertNotification(Title : String, Message : String){
            
            let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
         }
         */
        
    }

    

