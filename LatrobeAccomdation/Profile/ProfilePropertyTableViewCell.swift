//
//  ProfilePropertyTableViewCell.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 20/2/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit

class ProfilePropertyTableViewCell: UITableViewCell {
    @IBOutlet weak var prepertyImage: UIImageView!
    @IBOutlet weak var propertyAddress: UILabel!
    @IBOutlet weak var propertyRent: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
