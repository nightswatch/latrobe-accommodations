//
//  ProfileViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 23/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var segControl: UISegmentedControl!
    @IBOutlet weak var listedTabelview: UITableView!
    
    @IBOutlet weak var appliedTabelView: UITableView!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    
    @IBOutlet weak var appliedToButton: UIButton!
    
    @IBOutlet weak var postedByButton: UIButton!
    
    @IBOutlet weak var logInOutButton: UIButton!
    
    var postedProperties = [postedbyPropertyDetails]()
    var appliedProperties = [postedbyPropertyDetails]()
    var applicantArray = [String]()
    
    let db = Firestore.firestore()
    var propertyID:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let logoutButton = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: Selector("logoutClicked"))
//        self.navigationItem.rightBarButtonItem = logoutButton
//        postedByButton.layer.cornerRadius = 5
//        appliedToButton.layer.cornerRadius = 5
        
        
//        postedByButton.backgroundColor = .white
//        appliedToButton.backgroundColor = .lightGray
        profileImage.layer.borderWidth=1.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.clipsToBounds = true
        
        loadProfile()
        propertiesPostedByUser()
        propertiesAppliedByUser()
        //        loadingApplicant()
        print(applicantArray)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadProfile()
        propertiesPostedByUser()
        propertiesAppliedByUser()
        
//        postedByButton.backgroundColor = .white
//        appliedToButton.backgroundColor = .lightGray
    }
    
    override func viewDidAppear(_ animated: Bool) {
                userAuthentication()
        loadProfile()
        propertiesPostedByUser()
        propertiesAppliedByUser()
        
//        postedByButton.backgroundColor = .white
//        appliedToButton.backgroundColor = .lightGray
    }
    
    func loadProfile(){
        if Auth.auth().currentUser?.email! != nil{
            logInOutButton.setTitle("Log Out", for: UIControl.State.normal)
            db.collection("User").whereField("ID", isEqualTo: Auth.auth().currentUser?.email as! String).addSnapshotListener { (querySnapshot, error) in
                //                   self.initializePage()
//                self.appliedProperties = []
//                self.postedProperties = []
//                self.listedTabelview.isHidden = true
//                self.appliedTabelView.isHidden = false
//                self.appliedToButton.isEnabled = false
                //                self.appliedToButton.setTitle("You have not Applied to any Property", for: UIControl.State.normal)
//                self.postedByButton.isEnabled = false
                //                self.postedByButton.setTitle("You have not Posted any Property", for: UIControl.State.normal)
                if let e = error{
                    print("There was an error in retrieving data \(e)")
                    self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                }else {
                    if let snapshotDocument = querySnapshot?.documents{
                        for doc in snapshotDocument{
                            let data = doc.data()
                            if let profileImageURL = data["ProfilePicture"] as? String,
                                let profileFirstName = data["FirstName"] as? String,
                                let profileLastName = data["LastName"] as? String{
                                
                                //                            let propertyImageGet = UIImage(data: propertyImageData)
                                
                                self.profileImage.sd_setImage(with: URL(string: profileImageURL))
                                
                                self.firstNameLabel.text = profileFirstName
                                
                                self.lastNameLabel.text = profileLastName
                                
                                
                            }
                            
                            
                        }
                    }
                }
            }
        }
        else  if Auth.auth().currentUser?.email! == nil {
            
            initializePage()
        }
    }
    
    func propertiesPostedByUser(){
        
        if Auth.auth().currentUser?.email! != nil{
            logInOutButton.setTitle("Log Out", for: UIControl.State.normal)
            db.collection("Property")
                .order(by: "DateField", descending: true)
                .whereField("PostedBy", isEqualTo: Auth.auth().currentUser?.email! as! String)
                .addSnapshotListener
                {
                    (querySnapshot, error) in
                    self.postedProperties = []
                    self.postedProperties.count
                    if let e = error{
                        print("There was an error in retrieving data \(e)")
                        self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                    }else {
                        if let snapshotDocument = querySnapshot?.documents{
                            for doc in snapshotDocument{
                                let data = doc.data()
                                if let propertyAddress = data["Address"] as? String,
                                    let propertyRent = data["Rent"] as? Double,
                                    let propertyID = data["ID"] as? String,
                                    let propertyImageData = data["PropertyImage"] as? String,
                                    let propertyCampus = data["Campus"] as? String,
                                    let dateField = data["DateField"] as? Double{
                                    
                                    let propertyFirebase = postedbyPropertyDetails(propertyAddress: propertyAddress, propertyRent: propertyRent, propertyImage: propertyImageData, propertyCampus:propertyCampus , propertyID: propertyID, propertydateField: dateField)
                                    
                                    self.postedProperties.append(propertyFirebase)
                                    
                                    DispatchQueue.main.async {
                                        self.listedTabelview.reloadData()
                                        if self.postedProperties.count > 0 {
//                                            self.postedByButton.isEnabled = true
                                            //                                        self.postedByButton.setTitle("Properties Listed By You : \(self.postedProperties.count)", for: UIControl.State.normal)
                                        }else{
//                                            self.postedByButton.isEnabled = false
//                                            self.postedByButton.backgroundColor = .white
                                            //                                        self.postedByButton.setTitle("You have not Posted any Property\(self.appliedProperties.count)", for: UIControl.State.normal)
                                        }
                                        
                                    }
                                    self.listedTabelview.reloadData()
                                }
                            }
                            
                        }
                        self.listedTabelview.reloadData()
                    }
            }
        } else if Auth.auth().currentUser?.email! == nil{
            
            initializePage()
        }
    }
    
    
    
    func propertiesAppliedByUser(){
        
        if Auth.auth().currentUser?.email! != nil{
            logInOutButton.setTitle("Log Out", for: UIControl.State.normal)
            db.collection("Applications")
                .order(by: "DateField", descending: true)
                .whereField("TenantID", isEqualTo: Auth.auth().currentUser?.email! as! String)
                .addSnapshotListener
                {
                    (querySnapshot, error) in
                    self.appliedProperties = []
                    self.appliedProperties.count
                    if let e = error{
                        print("There was an error in retrieving data \(e)")
                        self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                    }else {
                        if let snapshotDocument = querySnapshot?.documents{
                            for doc in snapshotDocument{
                                let data = doc.data()
                                if let propertyAddress = data["Address"] as? String,
                                    let propertyRent = data["Rent"] as? Double,
                                    let propertyID = data["PropertyID"] as? String,
                                    let propertyImageData = data["ImageData"] as? String,
                                    let propertyCampus = data["Campus"] as? String,
                                    let dateField = data["DateField"] as? Double{
                                    
                                    let propertyFirebase = postedbyPropertyDetails(propertyAddress: propertyAddress, propertyRent: propertyRent, propertyImage: propertyImageData, propertyCampus:propertyCampus , propertyID: propertyID, propertydateField: dateField)
                                    
                                    self.appliedProperties.append(propertyFirebase)
                                    
                                    DispatchQueue.main.async {
                                        self.appliedTabelView.reloadData()
                                        if self.appliedProperties.count > 0 {
//                                            self.appliedToButton.isEnabled = true
                                            //                                        self.appliedToButton.setTitle("Properties You have Applied to : \(self.appliedProperties.count)", for: UIControl.State.normal)
                                        }else{
//                                            self.appliedToButton.isEnabled = false
//                                            self.appliedToButton.backgroundColor = .white
                                            //                                            self.appliedToButton.setTitle("You have not Applied to any Property\(self.appliedProperties.count)", for: UIControl.State.normal)
                                            self.appliedTabelView.reloadData()
                                        }
                                    }
                                    
                                }
                                self.appliedTabelView.reloadData()
                            }
                             self.appliedTabelView.reloadData()
                        }
                    }
                    self.appliedTabelView.reloadData()
            }
            
        } else if Auth.auth().currentUser?.email! == nil{
            
            initializePage()
        }
    }
    
    
    func loadingApplicant() {
        for properties in postedProperties{
            let propertyID = properties.id
            db.collection("Applications").whereField("PropertyID", isEqualTo: propertyID).addSnapshotListener { (querySnapshot, error) in
                
                if let e = error{
                    print("There was an error in retrieving Application \(e)")
                    self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                }else {
                    if let snapshotDocument = querySnapshot?.documents{
                        for doc in snapshotDocument{
                            let data = doc.data()
                            if let userID = data["TenantID"] as? String{
                                
                                self.applicantArray.append(userID)
                                
                            }
                            //                                            DispatchQueue.main.async {
                            //                                                self.listedTabelview.reloadData()
                            //                                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    
    
    func userAuthentication(){
        //            if Auth.auth().currentUser != nil {
        //                let alert = UIAlertController(title: "User Allowed to post", message: "", preferredStyle: UIAlertController.Style.alert)
        //               let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        //                alert.addAction(okButton)
        //                self.present(alert, animated: true, completion: nil)
        //
        //            } else
        if Auth.auth().currentUser == nil  {
            
            performSegue(withIdentifier: "ToWelcomePageFromProfile", sender: self)
            let alert = UIAlertController(title: "Please Login to post an ad", message: "", preferredStyle: UIAlertController.Style.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
            //            User Not logged in
        }
    }
    
    @IBAction func logoutPressed(_ sender: Any) {
        
        let firebaseAuth = Auth.auth()
        do {
            try Auth.auth().signOut()
            //LOGOUT BUTTON IS GOING TO THE ROOTVIEWCONTROLLER//
            userAuthentication()
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
    }
    
    
    
    func alertNotification(Title : String, Message : String){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == appliedTabelView){
            if appliedProperties.count != 0 {
                return (appliedProperties.count)
            }else{
                return 0
            }
        }
        if postedProperties.count != 0 {
            return (postedProperties.count)
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == appliedTabelView){
            let cell = tableView.dequeueReusableCell(withIdentifier: "propertListCell", for: indexPath)as! PropertyDetailsSecondaryViewCell
            
            cell.propertyAddress?.text = appliedProperties[indexPath.row].address
            cell.propertyRent?.text = "$\(String(Int(appliedProperties[indexPath.row].rent)))"
            cell.prepertyImage?.sd_setImage(with: URL(string: self.appliedProperties[indexPath.row].imageData))
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "propertListCell", for: indexPath)as! PropertyDetailsSecondaryViewCell
        
        cell.propertyAddress?.text = postedProperties[indexPath.row].address
        cell.propertyRent?.text = "$\(String(Int(postedProperties[indexPath.row].rent)))"
        cell.prepertyImage?.sd_setImage(with: URL(string: self.postedProperties[indexPath.row].imageData))
        cell.applicantCountLabel?.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == listedTabelview){
            propertyID = postedProperties[indexPath.row].id
            
            //        print("UUID = \(propertyID)")
            performSegue(withIdentifier: "toApplicantsPage", sender: nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toApplicantsPage" {
            let destinationVC = segue.destination as! ApplicantViewController
            destinationVC.propertyID = propertyID
            
        }
    }
    
    
    
    @IBAction func postedByPressed(_ sender: Any) {
        appliedTabelView.isHidden = true
        listedTabelview.isHidden = false
//        postedByButton.backgroundColor = .lightGray
//        appliedToButton.backgroundColor = .white
        //        self.performSegue(withIdentifier: "ToPostedByYou", sender: nil)
    }
    
    @IBAction func appliedToPressed(_ sender: Any) {
        appliedTabelView.isHidden = false
        listedTabelview.isHidden = true
//        postedByButton.backgroundColor = .white
//        appliedToButton.backgroundColor = .lightGray
        //        self.performSegue(withIdentifier: "AppliedTo", sender: nil)
    }
    
    @IBAction func segControlChanged(_ sender: Any) {
        if segControl.selectedSegmentIndex == 0{
            appliedTabelView.isHidden = false
            listedTabelview.isHidden = true
        }else if segControl.selectedSegmentIndex == 1{
            appliedTabelView.isHidden = true
            listedTabelview.isHidden = false
        }
    }
    
    func initializePage(){
        profileImage.image = nil
        firstNameLabel.text = "Please Login to Retrieve"
        lastNameLabel.text = "User Information"
//        appliedToButton.isEnabled = false
//        appliedToButton.backgroundColor = .white
//        postedByButton.isEnabled = false
//        postedByButton.backgroundColor = .white
        logInOutButton.setTitle("Log In", for: UIControl.State.normal)
//        listedTabelview.isHidden = true
//        appliedTabelView.isHidden = true
        
    }
    
}
