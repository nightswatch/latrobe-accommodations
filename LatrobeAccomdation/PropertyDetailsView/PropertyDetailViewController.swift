//
//  PropertyDetailViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 11/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces
import GoogleMaps
import NotificationBannerSwift
import EventKit

class PropertyDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var noPropertyTypeLabel: UILabel!
    
    @IBOutlet weak var noFurnishedLabel: UILabel!
    
    @IBOutlet weak var noFeatureLabel: UILabel!
    
    @IBOutlet weak var noAccessibilityLabel: UILabel!
    
    @IBOutlet weak var noTenantLabel: UILabel!
    
    @IBOutlet weak var noHomeRulesLabel: UILabel!
    @IBOutlet weak var accessibilitesCollection: UICollectionView!
    @IBOutlet weak var featuresCollection: UICollectionView!
    
    @IBOutlet weak var homeRulesCollection: UICollectionView!
    
    @IBOutlet weak var tenantCollection: UICollectionView!
    
    @IBOutlet weak var properyTypeCollection: UICollectionView!
    
    @IBOutlet weak var furnishedCollection: UICollectionView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var propertyImage: UIImageView!
    @IBOutlet weak var addressPropertyLabel: UILabel!
    @IBOutlet weak var rentTextField: UILabel!
    
    @IBOutlet weak var billsIncluded: UILabel!
    //    @IBOutlet weak var minimumStay: UILabel!
    @IBOutlet weak var maxStayLabel: UILabel!
    @IBOutlet weak var minStayLabel: UILabel!
    @IBOutlet weak var propertyAvailable: UILabel!
    @IBOutlet weak var propertyTitle: UITextField!
    @IBOutlet weak var propertyDescription: UITextView!
    
    @IBOutlet weak var inspectionLabel: UILabel!
    @IBOutlet weak var inspectionTime: UILabel!
    
    @IBOutlet weak var selectedCampus: UILabel!
    //    --------------Property Type Buttons-----------------
    @IBOutlet weak var apartmentButton: UIButton!
    @IBOutlet weak var studioAptButton: UIButton!
    @IBOutlet weak var sharedRoomButton: UIButton!
    @IBOutlet weak var privateRoomButton: UIButton!
    //    --------------Furnished Buttons-----------------
    @IBOutlet weak var bedsButton: UIButton!
    @IBOutlet weak var studyTableButton: UIButton!
    @IBOutlet weak var wardrobeButton: UIButton!
    @IBOutlet weak var kitchenAmenitiesButton: UIButton!
    //    --------------Features Buttons-----------------
    @IBOutlet weak var roomsButton: UIButton!
    @IBOutlet weak var bathroomsButton: UIButton!
    @IBOutlet weak var coolingButton: UIButton!
    @IBOutlet weak var wifiButton: UIButton!
    @IBOutlet weak var washingMachineButton: UIButton!
    @IBOutlet weak var heatingButton: UIButton!
    @IBOutlet weak var parkingButton: UIButton!
    @IBOutlet weak var wheelchairButton: UIButton!
    //    --------------Accessibilities Buttons-----------------
    @IBOutlet weak var publicTransportButton: UIButton!
    @IBOutlet weak var storeButton: UIButton!
    @IBOutlet weak var shoppingButton: UIButton!
    @IBOutlet weak var restaurantButton: UIButton!
    @IBOutlet weak var swimmingButton: UIButton!
    @IBOutlet weak var hospitalButton: UIButton!
    @IBOutlet weak var fuelStationButton: UIButton!
    @IBOutlet weak var gymButton: UIButton!
    //    --------------Tenant Buttons-----------------
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var noPreferenceButton: UIButton!
    //    --------------HomeRules Buttons-----------------
    @IBOutlet weak var petFriendlyButton: UIButton!
    @IBOutlet weak var noSmokingButton: UIButton!
    @IBOutlet weak var quitePlaceButton: UIButton!
    @IBOutlet weak var addressMapMarker: GMSMapView!
    
    @IBOutlet weak var applyButton: UIButton!
    
    @IBOutlet weak var addEventButton: UIButton!
    
    let db = Firestore.firestore()
    let banner = FloatingNotificationBanner(title: "Application Successful", subtitle: "Your Application for this property has been submitted", style: .success)
    let calendarBanner = FloatingNotificationBanner(title: "Added to Calendar", subtitle: "Your Inspection for this property has been added to your calendar", style: .success)
    
    //    --------------Property Type Variables Data-----------------
    var apartmentData: Int = 1
    var studioAptData: Int = 1
    var sharedRoomData: Int = 1
    var privateRoomData: Int = 1
    //    --------------Furnished Variables Data-----------------
    var bedsData : Int = 1
    var studyTableData : Int = 1
    var wardrobeData : Int = 1
    var kitchenAmenitiesData : Int = 1
    //    --------------Features Variables Data-----------------
    var roomsData : Int = 1
    var bathroomsData : Int = 1
    var coolingData : Int = 1
    var wifiData : Int = 1
    var washingMachineData : Int = 1
    var heatingData : Int = 1
    var parkingData : Int = 1
    var wheelchairData : Int = 1
    //    --------------Accessibilities Variables Data-----------------
    var publicTransportData : Int = 1
    var storeData : Int = 1
    var shoppingData : Int = 1
    var restaurantData : Int = 1
    var swimmingData : Int = 1
    var hospitalData : Int = 1
    var fuelstationData : Int = 1
    var gymData : Int = 1
    //    --------------Tenants Variables Data-----------------
    var maleData: Int = 1
    var femaleData: Int = 1
    var noPreferenceData: Int = 1
    //    --------------HomeRules Variable Data-----------------
    var petFriendlyData: Int = 1
    var noSmokingData: Int = 1
    var quitePlaceData: Int = 1
    //    -------------- bill variable data----------------
    var billData : Int = 1
    
    
    var propertyID : String = ""
    var postedByID : String = ""
    var address : String = ""
    var rent : Int = 0
    var imageData : String = ""
    
    var applicants = [String]()
    
    
    //--------------- Date variable ---------
    var myStartDate = Date()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressMapMarker.isUserInteractionEnabled = false
        loadProperties()
        scrollToTop()
        applyButton.isEnabled = true
        applyButton.layer.cornerRadius = 10
        addEventButton.layer.cornerRadius = 10
        noPropertyTypeLabel.isHidden = true
        noFurnishedLabel.isHidden = true
        noFeatureLabel.isHidden = true
        noAccessibilityLabel.isHidden = true
        noTenantLabel.isHidden = true
        noHomeRulesLabel.isHidden = true
//        addEventButton.layoutMargins.self = true
        addEventButton.layer.borderColor = #colorLiteral(red: 0, green: 0.5994044542, blue: 0.5334758759, alpha: 1)
        addEventButton.layer.borderWidth = 2
        addEventButton.backgroundColor = .systemBackground
        
        //        loadApplications()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadApplications()
        userAuthentication()
        addressMapMarker.isUserInteractionEnabled = false
        applyButton.isEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadApplications()
        userAuthentication()
        addressMapMarker.isUserInteractionEnabled = false
        applyButton.isEnabled = true
    }
    
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -self.scrollView.contentInset.top)
        self.scrollView.setContentOffset(desiredOffset, animated: true)
    }
    
    func loadApplications(){
        db.collection("Applications").order(by: "DateField").whereField("PropertyID", isEqualTo: propertyID).addSnapshotListener { (querySnapshot, error) in
            
            if let e = error{
                print("There was an error in retrieving data \(e)")
                self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
            }else {
                if let snapshotDocument = querySnapshot?.documents{
                    for doc in snapshotDocument{
                        let data = doc.data()
                        if let appliedTenantID = data["TenantID"] as? String{
                            
                            self.applicants.append(appliedTenantID)
                            let user = Auth.auth().currentUser?.email
                            if user != nil{
                            if self.applicants.contains((Auth.auth().currentUser?.email!)as! String){
                                self.applyButton.setTitle("Applied",for: .normal)
                                self.applyButton.isEnabled = false
                            }else if appliedTenantID != Auth.auth().currentUser?.email!{
                                self.applyButton.setTitle("Apply for Property",for: .normal)
                            }
                            }else {
                                self.userAuthentication()
                            }
                            DispatchQueue.main.async {
                                
                            }
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    
    func loadProperties(){
        
        
        db.collection("Property").whereField("ID", isEqualTo: propertyID).addSnapshotListener { (querySnapshot, error) in
            self.initializePage()
            if let e = error{
                print("There was an error in retrieving data \(e)")
                self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
            }else {
                if let snapshotDocument = querySnapshot?.documents{
                    for doc in snapshotDocument{
                        let data = doc.data()
                        if let propertyAddress = data["Address"] as? String,
                            let propertyRent = data["Rent"] as? Int,
                            let propertyImageData = data["PropertyImage"]as? String,
                            let propertyCampus = data["Campus"]as? String,
                            let apartmentData = data["Apartment"] as? Int,
                            let studioAptData = data["StudioApartment"] as? Int,
                            let sharedRoomData = data["SharedRoom"] as? Int,
                            let privateRoomData = data["PrivateRoom"] as? Int,
                            let bedsData = data["Beds"] as? Int,
                            let studyTableData = data["StudyTabel"] as? Int,
                            let wardrobeData = data["Wardrobe"] as? Int,
                            let kitchenAmenitiesData = data["KitchenAmenities"] as? Int,
                            let roomsData = data["Rooms"] as? Int,
                            let bathroomsData = data["Bathrooms"] as? Int,
                            let coolingData = data["Cooling"] as? Int,
                            let wifiData = data["Wifi"] as? Int,
                            let washingMachineData = data["WashingMachine"] as? Int,
                            let heatingData = data["Heating"] as? Int,
                            let parkingData = data["Parking"] as? Int,
                            let wheelchairData = data["WheelChair"] as? Int,
                            let publicTransportData = data["PublicTransport"] as? Int,
                            let storeData = data["Store"] as? Int,
                            let shoppingData = data["Shopping"] as? Int,
                            let restaurantData = data["Restaurant"] as? Int,
                            let swimmingData = data["Swimming"] as? Int,
                            let hospitalData = data["Hospital"] as? Int,
                            let fuelstationData = data["FuelStation"] as? Int,
                            let gymData = data["Gym"] as? Int,
                            let maleData = data["Male"] as? Int,
                            let femaleData = data["Female"] as? Int,
                            let noPreferenceData = data["NoPreference"] as? Int,
                            let petFriendlyData = data["PetFriendly"] as? Int,
                            let noSmokingData = data["NoSmoking"] as? Int,
                            let quitePlaceData = data["QuitePlace"] as? Int,
                            let latitude = data["Latitude"] as? Double,
                            let longitude = data["Longitude"] as? Double,
                            let postedBy = data["PostedBy"] as? String,
                            let maxStay = data["MaxStay"] as? String,
                            let minStay = data["MinStay"] as? String,
                            let billData = data["Bill"] as? Int,
                            let inspectionTime = data["InspectionTime"] as? String,
                            let inspectionDate = data["InspectionDate"] as? String,
                            let availableFrom = data["AvailableFrom"] as? String,
                            let propertyTitle = data["Title"] as? String,
                            let propertyDescription = data["Description"] as? String{
                            //                            let propertyImageGet = UIImage(data: propertyImageData)
                            
                            self.propertyImage.sd_setImage(with: URL(string: propertyImageData))
                            
                            self.addressPropertyLabel.text = propertyAddress
                            
                            self.rentTextField.text = "Per Week : $\(String(propertyRent))"
                            self.selectedCampus.text = propertyCampus
                            self.postedByID = postedBy
                            self.address = propertyAddress
                            self.rent = propertyRent
                            self.imageData = propertyImageData
                            self.maxStayLabel.text = maxStay
                            self.minStayLabel.text = minStay
                            self.inspectionLabel.text = inspectionDate
                            self.inspectionTime.text = inspectionTime
                            self.propertyAvailable.text = availableFrom
                            self.propertyTitle.text = propertyTitle
                            self.propertyDescription.text = propertyDescription
                            
                            if billData == 1{
                                self.billsIncluded.text = "Bills not Included"
                                self.billsIncluded.textColor = .red
                            }else if billData == 0{
                                self.billsIncluded.text = "Bills Included"
                                self.billsIncluded.textColor = UIColor (red: 76/255, green: 160/255, blue: 100/255, alpha: 1)
                            }

                            
                            //                            if apartmentData == 1   {
                            //                                self.apartmentButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.apartmentButton.tintColor = UIColor.black
                            //                            }
                            //                            if studioAptData == 1{
                            //                                self.studioAptButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.studioAptButton.tintColor = UIColor.black
                            //                            }
                            //                            if sharedRoomData == 1{
                            //                                self.sharedRoomButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.sharedRoomButton.tintColor = UIColor.black
                            //                            }
                            //                            if privateRoomData == 1
                            //                            {
                            //                                self.privateRoomButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.privateRoomButton.tintColor = UIColor.black
                            //                            }
                            //                            if bedsData  == 1{
                            //                                self.bedsButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.bedsButton.tintColor = UIColor.black
                            //                            }
                            //                            if studyTableData  == 1
                            //                            {
                            //                                self.studyTableButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.studyTableButton.tintColor = UIColor.black
                            //                            }
                            //                            if wardrobeData  == 1{
                            //
                            //                                self.wardrobeButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.wardrobeButton.tintColor = UIColor.black
                            //                            }
                            //                            if kitchenAmenitiesData  == 1
                            //                            {
                            //
                            //                                self.kitchenAmenitiesButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.kitchenAmenitiesButton.tintColor = UIColor.black
                            //                            }
                            //                            if roomsData  == 1{
                            //
                            //
                            //                                self.roomsButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.roomsButton.tintColor = UIColor.black
                            //                            }
                            //                            if bathroomsData  == 1
                            //                            {
                            //
                            //                                self.bathroomsButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.bathroomsButton.tintColor = UIColor.black
                            //                            }
                            //                            if coolingData  == 1{
                            //
                            //                                self.coolingButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.coolingButton.tintColor = UIColor.black
                            //                            }
                            //                            if wifiData  == 1{
                            //
                            //                                self.wifiButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.wifiButton.tintColor = UIColor.black
                            //                            }
                            //                            if washingMachineData  == 1{
                            //
                            //                                self.washingMachineButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.washingMachineButton.tintColor = UIColor.black
                            //                            }
                            //                            if heatingData  == 1{
                            //
                            //                                self.heatingButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.heatingButton.tintColor = UIColor.black
                            //                            }
                            //                            if parkingData  == 1
                            //                            {
                            //
                            //                                self.parkingButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.parkingButton.tintColor = UIColor.black
                            //                            }
                            //                            if wheelchairData  == 1{
                            //
                            //                                self.wheelchairButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.wheelchairButton.tintColor = UIColor.black
                            //                            }
                            //                            if publicTransportData  == 1
                            //                            {
                            //
                            //
                            //                                self.publicTransportButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.publicTransportButton.tintColor = UIColor.black
                            //                            }
                            //                            if storeData  == 1
                            //                            {
                            //
                            //                                self.storeButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.storeButton.tintColor = UIColor.black
                            //                            }
                            //                            if shoppingData  == 1{
                            //
                            //                                self.shoppingButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.shoppingButton.tintColor = UIColor.black
                            //                            }
                            //                            if restaurantData  == 1{
                            //
                            //                                self.restaurantButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.restaurantButton.tintColor = UIColor.black
                            //                            }
                            //                            if swimmingData  == 1{
                            //
                            //                                self.swimmingButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.swimmingButton.tintColor = UIColor.black
                            //                            }
                            //                            if hospitalData  == 1{
                            //
                            //                                self.hospitalButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.hospitalButton.tintColor = UIColor.black
                            //                            }
                            //                            if fuelstationData  == 1{
                            //
                            //                                self.fuelStationButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.fuelStationButton.tintColor = UIColor.black
                            //                            }
                            //                            if gymData  == 1{
                            //
                            //                                self.gymButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.gymButton.tintColor = UIColor.black
                            //                            }
                            //                            if maleData == 1{
                            //
                            //
                            //                                self.maleButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.maleButton.tintColor = UIColor.black
                            //                            }
                            //                            if femaleData == 1{
                            //
                            //                                self.femaleButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.femaleButton.tintColor = UIColor.black
                            //                            }
                            //                            if noPreferenceData == 1{
                            //
                            //                                self.noPreferenceButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.noPreferenceButton.tintColor = UIColor.black
                            //                            }
                            //                            if petFriendlyData == 1{
                            //
                            //
                            //                                self.petFriendlyButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.petFriendlyButton.tintColor = UIColor.black
                            //                            }
                            //                            if noSmokingData == 1{
                            //
                            //                                self.noSmokingButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.noSmokingButton.tintColor = UIColor.black
                            //                            }
                            //                            if quitePlaceData == 1{
                            //
                            //                                self.quitePlaceButton.tintColor = UIColor.lightGray
                            //                            }else{
                            //                                self.quitePlaceButton.tintColor = UIColor.black
                            //                            }
                            
                            
                            let cord2D = CLLocationCoordinate2D(latitude: (latitude), longitude: (longitude))
                            
                            let marker = GMSMarker()
                            marker.position =  cord2D
                            
                            //                            marker.snippet = place.name
                            //                            getAddress = String(place.name!)
                            
                            //let markerImage = UIImage(named: "icon_offer_pickup")!
                            //let markerView = UIImageView(image: markerImage)
                            //marker.iconView = markerView
                            marker.map = self.addressMapMarker
                            
                            self.addressMapMarker.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
                            
                            DispatchQueue.main.async {
                                
                            }
                            
                        }
                    }
                    
                }
            }
        }
    }
    
    
    
    func initializePage(){
        
        self.addressPropertyLabel.text = ""
        self.rentTextField.text = ""
        self.propertyImage.image = #imageLiteral(resourceName: "add_image_here_customisable_design_create_your_own_postcard-r713f152f597f4e47b56c30f784af59f0_vgbaq_8byvr_540")
        
        //        apartmentButton.tintColor = UIColor.gray
        //        studioAptButton.tintColor = UIColor.gray
        //        sharedRoomButton.tintColor = UIColor.gray
        //        privateRoomButton.tintColor = UIColor.gray
        
        //        bedsButton.tintColor = UIColor.gray
        //        studyTableButton.tintColor = UIColor.gray
        //        wardrobeButton.tintColor = UIColor.gray
        //        kitchenAmenitiesButton.tintColor = UIColor.gray
        
        //        roomsButton.tintColor = UIColor.gray
        //        bathroomsButton.tintColor = UIColor.gray
        //        coolingButton.tintColor = UIColor.gray
        //        wifiButton.tintColor = UIColor.gray
        //        washingMachineButton.tintColor = UIColor.gray
        //        heatingButton.tintColor = UIColor.gray
        //        parkingButton.tintColor = UIColor.gray
        //        wheelchairButton.tintColor = UIColor.gray
        
        //        publicTransportButton.tintColor = UIColor.gray
        //        storeButton.tintColor = UIColor.gray
        //        shoppingButton.tintColor = UIColor.gray
        //        restaurantButton.tintColor = UIColor.gray
        //        swimmingButton.tintColor = UIColor.gray
        //        hospitalButton.tintColor = UIColor.gray
        //        fuelStationButton.tintColor = UIColor.gray
        //        gymButton.tintColor = UIColor.gray
        
        //        maleButton.tintColor = UIColor.gray
        //        femaleButton.tintColor = UIColor.gray
        //        noPreferenceButton.tintColor = UIColor.gray
        
        //        petFriendlyButton.tintColor = UIColor.gray
        //        noSmokingButton.tintColor = UIColor.gray
        //        quitePlaceButton.tintColor = UIColor.gray
        
        //    --------------Property Type variablesData-----------------
        apartmentData = 1
        studioAptData = 1
        sharedRoomData = 1
        privateRoomData = 1
        //    --------------Furnished variablesData-----------------
        bedsData  = 1
        studyTableData  = 1
        wardrobeData  = 1
        kitchenAmenitiesData  = 1
        //    --------------Features variablesData-----------------
        roomsData  = 1
        bathroomsData  = 1
        coolingData  = 1
        wifiData  = 1
        washingMachineData  = 1
        heatingData  = 1
        parkingData  = 1
        wheelchairData  = 1
        //    --------------Accessibilities variablesData-----------------
        publicTransportData  = 1
        storeData  = 1
        shoppingData  = 1
        restaurantData  = 1
        swimmingData  = 1
        hospitalData  = 1
        fuelstationData  = 1
        gymData  = 1
        //    --------------Tenants variablesData-----------------
        maleData = 1
        femaleData = 1
        noPreferenceData = 1
        //    --------------HomeRules Buttons-----------------
        petFriendlyData = 1
        noSmokingData = 1
        quitePlaceData = 1
        
        
        
    }
    
    func alertNotification(Title : String, Message : String){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func settingUpIcons(){
        
        
        if apartmentData == 1   {
            apartmentButton.tintColor = UIColor.gray
        }else{
            apartmentButton.tintColor = UIColor.black
        }
        if studioAptData == 1{
            studioAptButton.tintColor = UIColor.gray
        }else{
            studioAptButton.tintColor = UIColor.black
        }
        if sharedRoomData == 1{
            sharedRoomButton.tintColor = UIColor.gray
        }else{
            sharedRoomButton.tintColor = UIColor.black
        }
        if privateRoomData == 1
        {
            privateRoomButton.tintColor = UIColor.gray
        }else{
            privateRoomButton.tintColor = UIColor.black
        }
        if bedsData  == 1{
            bedsButton.tintColor = UIColor.gray
        }else{
            bedsButton.tintColor = UIColor.black
        }
        if studyTableData  == 1
        {
            studyTableButton.tintColor = UIColor.gray
        }else{
            studyTableButton.tintColor = UIColor.black
        }
        if wardrobeData  == 1{
            
            wardrobeButton.tintColor = UIColor.gray
        }else{
            wardrobeButton.tintColor = UIColor.black
        }
        if kitchenAmenitiesData  == 1
        {
            
            kitchenAmenitiesButton.tintColor = UIColor.gray
        }else{
            kitchenAmenitiesButton.tintColor = UIColor.black
        }
        if roomsData  == 1{
            
            
            roomsButton.tintColor = UIColor.gray
        }else{
            roomsButton.tintColor = UIColor.black
        }
        if bathroomsData  == 1
        {
            
            bathroomsButton.tintColor = UIColor.gray
        }else{
            bathroomsButton.tintColor = UIColor.black
        }
        if coolingData  == 1{
            
            coolingButton.tintColor = UIColor.gray
        }else{
            coolingButton.tintColor = UIColor.black
        }
        if wifiData  == 1{
            
            wifiButton.tintColor = UIColor.gray
        }else{
            wifiButton.tintColor = UIColor.black
        }
        if washingMachineData  == 1{
            
            washingMachineButton.tintColor = UIColor.gray
        }else{
            washingMachineButton.tintColor = UIColor.black
        }
        if heatingData  == 1{
            
            heatingButton.tintColor = UIColor.gray
        }else{
            heatingButton.tintColor = UIColor.black
        }
        if parkingData  == 1
        {
            
            parkingButton.tintColor = UIColor.gray
        }else{
            parkingButton.tintColor = UIColor.black
        }
        if wheelchairData  == 1{
            
            wheelchairButton.tintColor = UIColor.gray
        }else{
            wheelchairButton.tintColor = UIColor.black
        }
        if publicTransportData  == 1
        {
            
            
            publicTransportButton.tintColor = UIColor.gray
        }else{
            publicTransportButton.tintColor = UIColor.black
        }
        if storeData  == 1
        {
            
            storeButton.tintColor = UIColor.gray
        }else{
            storeButton.tintColor = UIColor.black
        }
        if shoppingData  == 1{
            
            shoppingButton.tintColor = UIColor.gray
        }else{
            shoppingButton.tintColor = UIColor.black
        }
        if restaurantData  == 1{
            
            restaurantButton.tintColor = UIColor.gray
        }else{
            restaurantButton.tintColor = UIColor.black
        }
        if swimmingData  == 1{
            
            swimmingButton.tintColor = UIColor.gray
        }else{
            swimmingButton.tintColor = UIColor.black
        }
        if hospitalData  == 1{
            
            hospitalButton.tintColor = UIColor.gray
        }else{
            hospitalButton.tintColor = UIColor.black
        }
        if fuelstationData  == 1{
            
            fuelStationButton.tintColor = UIColor.gray
        }else{
            fuelStationButton.tintColor = UIColor.black
        }
        if gymData  == 1{
            
            gymButton.tintColor = UIColor.gray
        }else{
            gymButton.tintColor = UIColor.black
        }
        if maleData == 1{
            
            
            maleButton.tintColor = UIColor.gray
        }else{
            maleButton.tintColor = UIColor.black
        }
        if femaleData == 1{
            
            femaleButton.tintColor = UIColor.gray
        }else{
            femaleButton.tintColor = UIColor.black
        }
        if noPreferenceData == 1{
            
            noPreferenceButton.tintColor = UIColor.gray
        }else{
            noPreferenceButton.tintColor = UIColor.black
        }
        if petFriendlyData == 1{
            
            
            petFriendlyButton.tintColor = UIColor.gray
        }else{
            petFriendlyButton.tintColor = UIColor.black
        }
        if noSmokingData == 1{
            
            noSmokingButton.tintColor = UIColor.gray
        }else{
            noSmokingButton.tintColor = UIColor.black
        }
        if noSmokingData == 1{
            
            quitePlaceButton.tintColor = UIColor.gray
        }else{
            quitePlaceButton.tintColor = UIColor.black
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var cellcount = 0
        if(collectionView == accessibilitesCollection){
            if publicTransportData == 0{
                cellcount = cellcount + 1
            }
            if storeData == 0{
                cellcount = cellcount + 1
            }
            if shoppingData == 0{
                cellcount = cellcount + 1
            }
            if restaurantData == 0{
                cellcount = cellcount + 1
            }
            if swimmingData == 0{
                cellcount = cellcount + 1
            }
            if hospitalData == 0{
                cellcount = cellcount + 1
            }
            if fuelstationData == 0{
                cellcount = cellcount + 1
            }
            if gymData == 0{
                cellcount = cellcount + 1
            }
            if cellcount == 0{

                noAccessibilityLabel.isHidden = false
            }
            return cellcount
        }
        
        if(collectionView == featuresCollection){
            if roomsData == 0{
                cellcount = cellcount + 1
            }
            if bathroomsData == 0{
                cellcount = cellcount + 1
            }
            if coolingData == 0{
                cellcount = cellcount + 1
            }
            if wifiData == 0{
                cellcount = cellcount + 1
            }
            if washingMachineData == 0{
                cellcount = cellcount + 1
            }
            if heatingData == 0{
                cellcount = cellcount + 1
            }
            if parkingData == 0{
                cellcount = cellcount + 1
            }
            if wheelchairData == 0{
                cellcount = cellcount + 1
            }
            if cellcount == 0{
                noFeatureLabel.isHidden = false

            }
            return cellcount
        }
        
        if(collectionView == homeRulesCollection){
            if petFriendlyData == 0{
                cellcount = cellcount + 1
            }
            if noSmokingData == 0{
                cellcount = cellcount + 1
            }
            if quitePlaceData == 0{
                cellcount = cellcount + 1
            }
            if cellcount == 0{
        
                noHomeRulesLabel.isHidden = false
            }
            
            return cellcount
        }
        if(collectionView == tenantCollection){
            if maleData == 0{
                cellcount = cellcount + 1
            }
            if femaleData == 0{
                cellcount = cellcount + 1
            }
            if noPreferenceData == 0{
                cellcount = cellcount + 1
            }
            if cellcount == 0{
            
                noTenantLabel.isHidden = false
               
            }
            
            return cellcount
        }
        if(collectionView == furnishedCollection){
            if bedsData == 0{
                cellcount = cellcount + 1
            }
            if studyTableData == 0{
                cellcount = cellcount + 1
            }
            if wardrobeData == 0{
                cellcount = cellcount + 1
            }
            if kitchenAmenitiesData == 0{
                cellcount = cellcount + 1
            }
            if cellcount == 0{
            
                noFurnishedLabel.isHidden = false
              
            }
            
            return cellcount
        }
        if apartmentData == 0{
            cellcount = cellcount + 1
        }
        if studioAptData == 0{
            cellcount = cellcount + 1
        }
        if sharedRoomData == 0{
            cellcount = cellcount + 1
        }
        if privateRoomData == 0{
            cellcount = cellcount + 1
        }
        if cellcount == 0{
            noPropertyTypeLabel.isHidden = false
        }
        
        return cellcount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == accessibilitesCollection){
            let furnishedrow = indexPath.row
            switch furnishedrow {
            case 0:
                if publicTransportData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "publiccell", for: indexPath)
                    publicTransportData = 1
                    return cell
                }
                else if storeData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storecell", for: indexPath)
                    storeData = 1
                    return cell
                }
                else if shoppingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shoppingcell", for: indexPath)
                    shoppingData = 1
                    return cell
                    
                }
                else if restaurantData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restcell", for: indexPath)
                    restaurantData = 1
                    return cell
                }
                else if swimmingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "swimmingcell", for: indexPath)
                    swimmingData = 1
                    return cell
                }
                else if hospitalData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hospitalcell", for: indexPath)
                    hospitalData = 1
                    return cell
                    
                }
                else if fuelstationData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelcell", for: indexPath)
                    fuelstationData = 1
                    return cell
                }
                else if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
                
            case 1:
                if storeData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storecell", for: indexPath)
                    storeData = 1
                    return cell
                }
                else if shoppingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shoppingcell", for: indexPath)
                    shoppingData = 1
                    return cell
                    
                }
                else if restaurantData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restcell", for: indexPath)
                    restaurantData = 1
                    return cell
                }
                else if swimmingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "swimmingcell", for: indexPath)
                    swimmingData = 1
                    return cell
                }
                else if hospitalData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hospitalcell", for: indexPath)
                    hospitalData = 1
                    return cell
                    
                }
                else if fuelstationData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelcell", for: indexPath)
                    fuelstationData = 1
                    return cell
                }
                else if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
            case 2:
                if shoppingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shoppingcell", for: indexPath)
                    shoppingData = 1
                    return cell
                    
                }
                else if restaurantData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restcell", for: indexPath)
                    restaurantData = 1
                    return cell
                }
                else if swimmingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "swimmingcell", for: indexPath)
                    swimmingData = 1
                    return cell
                }
                else if hospitalData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hospitalcell", for: indexPath)
                    hospitalData = 1
                    return cell
                    
                }
                else if fuelstationData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelcell", for: indexPath)
                    fuelstationData = 1
                    return cell
                }
                else if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
            case 3:
                if restaurantData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restcell", for: indexPath)
                    restaurantData = 1
                    return cell
                }
                else if swimmingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "swimmingcell", for: indexPath)
                    swimmingData = 1
                    return cell
                }
                else if hospitalData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hospitalcell", for: indexPath)
                    hospitalData = 1
                    return cell
                    
                }
                else if fuelstationData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelcell", for: indexPath)
                    fuelstationData = 1
                    return cell
                }
                else if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
                
            case 4:
                if swimmingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "swimmingcell", for: indexPath)
                    swimmingData = 1
                    return cell
                }
                else if hospitalData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hospitalcell", for: indexPath)
                    hospitalData = 1
                    return cell
                    
                }
                else if fuelstationData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelcell", for: indexPath)
                    fuelstationData = 1
                    return cell
                }
                else if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
            case 5:
                if hospitalData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hospitalcell", for: indexPath)
                    hospitalData = 1
                    return cell
                    
                }
                else if fuelstationData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelcell", for: indexPath)
                    fuelstationData = 1
                    return cell
                }
                else if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
            case 6:
                if fuelstationData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelcell", for: indexPath)
                    fuelstationData = 1
                    return cell
                }
                else if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
            case 7:
                if gymData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gymcell", for: indexPath)
                    gymData = 1
                    return cell
                }
                
            //        return HomeCollectionViewCell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "roomscell", for: indexPath)
                return cell
            }
            //        return UICollectionViewCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "roomscell", for: indexPath)
            return cell
            
        }
        
        
        if(collectionView == featuresCollection){
            let furnishedrow = indexPath.row
            switch furnishedrow {
            case 0:
                if roomsData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "roomscell", for: indexPath)
                    roomsData = 1
                    return cell
                }
                else if bathroomsData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bathroomscell", for: indexPath)
                    bathroomsData = 1
                    return cell
                }
                else if coolingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coolingcell", for: indexPath)
                    coolingData = 1
                    return cell
                    
                }
                else if wifiData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wificell", for: indexPath)
                    wifiData = 1
                    return cell
                }
                else if washingMachineData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "parkingcell", for: indexPath)
                    washingMachineData = 1
                    return cell
                }
                else if heatingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heatingcell", for: indexPath)
                    heatingData = 1
                    return cell
                    
                }
                else if parkingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washingcell", for: indexPath)
                    parkingData = 1
                    return cell
                }
                else if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
                
            case 1:
                if bathroomsData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bathroomscell", for: indexPath)
                    bathroomsData = 1
                    return cell
                }
                else if coolingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coolingcell", for: indexPath)
                    coolingData = 1
                    return cell
                    
                }
                else if wifiData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wificell", for: indexPath)
                    wifiData = 1
                    return cell
                }
                else if washingMachineData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "parkingcell", for: indexPath)
                    washingMachineData = 1
                    return cell
                }
                else if heatingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heatingcell", for: indexPath)
                    heatingData = 1
                    return cell
                    
                }
                else if parkingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washingcell", for: indexPath)
                    parkingData = 1
                    return cell
                }
                else if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
            case 2:
                if coolingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coolingcell", for: indexPath)
                    coolingData = 1
                    return cell
                    
                }
                else if wifiData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wificell", for: indexPath)
                    wifiData = 1
                    return cell
                }
                else if washingMachineData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "parkingcell", for: indexPath)
                    washingMachineData = 1
                    return cell
                }
                else if heatingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heatingcell", for: indexPath)
                    heatingData = 1
                    return cell
                    
                }
                else if parkingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washingcell", for: indexPath)
                    parkingData = 1
                    return cell
                }
                else if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
            case 3:
                if wifiData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wificell", for: indexPath)
                    wifiData = 1
                    return cell
                }
                else if washingMachineData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "parkingcell", for: indexPath)
                    washingMachineData = 1
                    return cell
                }
                else if heatingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heatingcell", for: indexPath)
                    heatingData = 1
                    return cell
                    
                }
                else if parkingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washingcell", for: indexPath)
                    parkingData = 1
                    return cell
                }
                else if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
                
            case 4:
                if washingMachineData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "parkingcell", for: indexPath)
                    washingMachineData = 1
                    return cell
                }
                else if heatingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heatingcell", for: indexPath)
                    heatingData = 1
                    return cell
                    
                }
                else if parkingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washingcell", for: indexPath)
                    parkingData = 1
                    return cell
                }
                else if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
            case 5:
                if heatingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heatingcell", for: indexPath)
                    heatingData = 1
                    return cell
                    
                }
                else if parkingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washingcell", for: indexPath)
                    parkingData = 1
                    return cell
                }
                else if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
            case 6:
                if parkingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washingcell", for: indexPath)
                    parkingData = 1
                    return cell
                }
                else if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
            case 7:
                if wheelchairData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wheelchaircell", for: indexPath)
                    wheelchairData = 1
                    return cell
                }
                
            //        return HomeCollectionViewCell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "roomscell", for: indexPath)
                return cell
            }
            //        return UICollectionViewCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "roomscell", for: indexPath)
            return cell
            
        }
        
        
        
        if(collectionView == homeRulesCollection){
            let homerow = indexPath.row
            switch homerow {
            case 0:
                if petFriendlyData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "petcell", for: indexPath)
                    petFriendlyData = 1
                    return cell
                }
                else if noSmokingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "smokingcell", for: indexPath)
                    noSmokingData = 1
                    return cell
                }
                else if quitePlaceData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "quitecell", for: indexPath)
                    quitePlaceData = 1
                    return cell
                    
                }
                
                
            case 1:
                if noSmokingData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "smokingcell", for: indexPath)
                    noSmokingData = 1
                    return cell
                }
                else if quitePlaceData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "quitecell", for: indexPath)
                    quitePlaceData = 1
                    return cell
                    
                }
            case 2:
                if quitePlaceData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "quitecell", for: indexPath)
                    quitePlaceData = 1
                    return cell
                    
                }
                
            //        return HomeCollectionViewCell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "malecell", for: indexPath)
                return cell
            }
            //        return UICollectionViewCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "malecell", for: indexPath)
            return cell
            
        }
        if(collectionView == tenantCollection){
            let tenantrow = indexPath.row
            switch tenantrow {
            case 0:
                if maleData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "malecell", for: indexPath)
                    maleData = 1
                    return cell
                }
                else if femaleData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "femalecell", for: indexPath)
                    femaleData = 1
                    return cell
                }
                else if noPreferenceData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nocell", for: indexPath)
                    noPreferenceData = 1
                    return cell
                    
                }
                
                
            case 1:
                if femaleData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "femalecell", for: indexPath)
                    femaleData = 1
                    return cell
                }
                else if noPreferenceData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nocell", for: indexPath)
                    noPreferenceData = 1
                    return cell
                    
                }
            case 2:
                if noPreferenceData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nocell", for: indexPath)
                    noPreferenceData = 1
                    return cell
                    
                }
                
            //        return HomeCollectionViewCell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "malecell", for: indexPath)
                return cell
            }
            //        return UICollectionViewCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "malecell", for: indexPath)
            return cell
            
        }
        if(collectionView == furnishedCollection){
            let furnishedrow = indexPath.row
            switch furnishedrow {
            case 0:
                if bedsData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bedscell", for: indexPath)
                    bedsData = 1
                    return cell
                }
                else if studyTableData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "studycell", for: indexPath)
                    studyTableData = 1
                    return cell
                }
                else if wardrobeData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wardrobecell", for: indexPath)
                    
                    wardrobeData = 1
                    return cell
                    
                }
                else if kitchenAmenitiesData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "kitchencell", for: indexPath)
                    kitchenAmenitiesData = 1
                    return cell
                }
                
            case 1:
                if studyTableData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "studycell", for: indexPath)
                    studyTableData = 1
                    return cell
                }
                else if wardrobeData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wardrobecell", for: indexPath)
                    wardrobeData = 1
                    return cell
                    
                }
                else if kitchenAmenitiesData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "kitchencell", for: indexPath)
                    kitchenAmenitiesData = 1
                    return cell
                }
            case 2:
                if wardrobeData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wardrobecell", for: indexPath)
                    wardrobeData = 1
                    return cell
                }
                else if kitchenAmenitiesData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "kitchencell", for: indexPath)
                    kitchenAmenitiesData = 1
                    return cell
                }
            case 3:
                if kitchenAmenitiesData == 0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "kitchencell", for: indexPath)
                    kitchenAmenitiesData = 1
                    return cell
                }
                
                
                
            //        return HomeCollectionViewCell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bedscell", for: indexPath)
                return cell
            }
            //        return UICollectionViewCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bedscell", for: indexPath)
            return cell
            
        }
        
        let row = indexPath.row
        switch row {
        case 0:
            if apartmentData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "apartmentcell", for: indexPath)
                apartmentData = 1
                return cell
            }
            else if studioAptData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "studiocell", for: indexPath)
                studioAptData = 1
                return cell
            }
            else if sharedRoomData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sharedcell", for: indexPath)
                
                sharedRoomData = 1
                return cell
                
            }
            else if privateRoomData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "privatecell", for: indexPath)
                privateRoomData = 1
                return cell
            }
            
        case 1:
            if studioAptData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "studiocell", for: indexPath)
                studioAptData = 1
                return cell
            }
            else if sharedRoomData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sharedcell", for: indexPath)
                sharedRoomData = 1
                return cell
                
            }
            else if privateRoomData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "privatecell", for: indexPath)
                privateRoomData = 1
                return cell
            }
        case 2:
            if sharedRoomData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sharedcell", for: indexPath)
                sharedRoomData = 1
                return cell
            }
            else if privateRoomData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "privatecell", for: indexPath)
                privateRoomData = 1
                return cell
            }
        case 3:
            if privateRoomData == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "privatecell", for: indexPath)
                privateRoomData = 1
                return cell
            }
            
            
            
        //        return HomeCollectionViewCell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "apartmentcell", for: indexPath)
            return cell
        }
        //        return UICollectionViewCell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "apartmentcell", for: indexPath)
        return cell
        
    }
    
    
    @IBAction func addEventTapped(_ sender: Any) {
        
        //------------- used dateFormatter to convert the String retrieved from the inpectionLabel.text into the Date() type date
            let dateFormatter = DateFormatter()
        
        //-----------  unicode LDML: Dates
        dateFormatter.dateFormat = "dd MMM yyyy h:mm a"
        //------------ check the local timezone?
        dateFormatter.timeZone = TimeZone(abbreviation: "AEDT")
        //------------used the start and end sopt to define the range of a piece of a string (followed the dateformat to generate the Date
//        inspectionLabel.text = "Inspection Data and Time : \(inspectionLabel.text!) \(inspectionTime.text!)"
        let  StartDateString = "\(inspectionLabel.text!) \(inspectionTime.text!)"
        print(StartDateString)
        if let myStartDateString = StartDateString as? String {
//            let myStartDateSubString = String(myStartDateString.prefix(17))
            let start = myStartDateString.index(myStartDateString.startIndex, offsetBy: 0)
            
//            if myStartDateString.count == 
            let end = myStartDateString.index(myStartDateString.endIndex, offsetBy: -12)
            let range = start...end
            let myStartDateSubString = myStartDateString[range]
            
            print(myStartDateSubString)
            print(myStartDateSubString)
            print(myStartDateSubString)
            print(myStartDateSubString)
            print(myStartDateSubString)
            print(myStartDateSubString)
            print(myStartDateSubString)
        //------generate the Date() from a piece of string
            myStartDate = dateFormatter.date(from: String(myStartDateSubString))!
            
        }
        
        self.calendarBanner.show()
//        self.alertNotification(Title: "Added to Calendar", Message: "The Inspection has been successfully added to Your Calendar")
            
            //-----------used the start date to add an event to calendar
            //-------authorization steps
             //---------- call the EKeventStore
            let eventStore = EKEventStore()
            //---------- request the authorization
            eventStore.requestAccess(to: .event) { (granted, error) in
                if (granted) && (error == nil) {
                    print("granted" + " no error")
 //------ authorization "granted" +  no error
                    //-------- get the event from the eventStore
                    let event = EKEvent(eventStore: eventStore)
                    //set up the properties of the event
                    event.title = "Inspection time"
                    //--------- use the date as the start point
                    event.startDate = self.myStartDate
                    //--------- inspection time will always be 15min
                    event.endDate = event.startDate.addingTimeInterval(15*60)
                    // use the property address as the note
                    let addressNote = self.addressPropertyLabel.text
                    if let eventAddressNote = addressNote as? String{
                    event.notes = eventAddressNote
                    }
                    //----------- use the default calendar for the new event
                    // ------ use
                    event.calendar = eventStore.defaultCalendarForNewEvents
                    do {
                        try eventStore.save(event, span: .thisEvent)
                    } catch let error as NSError {
                        print("you get an error")
                    }
                    print("New event created")
                    
//
                } else {

                    print("error is \(error)")



                }
                
            }
            
            
// ------------------Code to disable a button //
//        addEventButton.isHidden = true
//
//       addEventButton.isEnabled = false
//       addEventButton.setTitle("Added", for: .normal)
//
            
        
    }
    
    
    
    
    @IBAction func applyPressed(_ sender: Any) {
        
        //        userAuthentication()
        if Auth.auth().currentUser == nil  {
            performSegue(withIdentifier: "ToWelcomPageFromApplication", sender: self)
        }
        else{
            
            if  let tenantID = Auth.auth().currentUser?.email!{
                if propertyID != "",
                    postedByID != "",
                    address != "",
                    rent != 0,
                    imageData != "",
                    let campus = selectedCampus.text{
                    self.db.collection("Applications").addDocument(data: ["PropertyID" : propertyID,
                                                                          "TenantID" : tenantID,
                                                                          "LandlordID" : postedByID,
                                                                          "DateField": Date().timeIntervalSince1970,
                                                                          "Address": address,
                                                                          "Rent": rent,
                                                                          "ImageData": imageData,
                                                                          "Campus": campus])
                    { (error) in
                        if let e = error{
                            print("There was an isssue saving the property, \(e)")
                            self.alertNotification(Title: "Not Posted!", Message: "Not saved to the database")
                            
                        }
                        else{
                            print("Successfully saved the Property Details")
                            
                            self.banner.show()
                            self.applyButton.setTitle("Applied",for: .normal)
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    func userAuthentication(){
        //        if Auth.auth().currentUser != nil {
        //            let alert = UIAlertController(title: "User Allowed to post", message: "", preferredStyle: UIAlertController.Style.alert)
        //           let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        //            alert.addAction(okButton)
        //            self.present(alert, animated: true, completion: nil)
        //
        //        } else
        if Auth.auth().currentUser == nil  {
            performSegue(withIdentifier: "ToWelcomPageFromApplication", sender: self)
            let alert = UIAlertController(title: "Please Login to post an ad", message: "", preferredStyle: UIAlertController.Style.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
            //            User Not logged in
        }
    }
    
}

