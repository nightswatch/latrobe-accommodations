//
//  PropertyDetailsRealm.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 18/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import Foundation
import RealmSwift

class property: Object {

//    @objc dynamic var propertyTitle : String? = nil
    
    @objc dynamic var availableFromTextField : String? = nil
    @objc dynamic var address : String? = nil
    @objc dynamic var selectedCampus : String? = nil
    @objc dynamic var id : String? = nil
    @objc dynamic var imageData : String? = nil
//    @objc dynamic var image : UIImage? = nil
    @objc dynamic var rent = 0000000
    @objc dynamic var apartmentData = 1
    @objc dynamic var studioAptData = 1
    @objc dynamic var sharedRoomData = 1
    @objc dynamic var privateRoomData = 1
    @objc dynamic var bedsData  = 1
    @objc dynamic var studyTableData  = 1
    @objc dynamic var wardrobeData  = 1
    @objc dynamic var kitchenAmenitiesData  = 1
    @objc dynamic var roomsData  = 1
    @objc dynamic var bathroomsData  = 1
    @objc dynamic var coolingData  = 1
    @objc dynamic var wifiData  = 1
    @objc dynamic var washingMachineData  = 1
    @objc dynamic var heatingData  = 1
    @objc dynamic var parkingData  = 1
    @objc dynamic var wheelchairData  = 1
    @objc dynamic var publicTransportData  = 1
    @objc dynamic var storeData  = 1
    @objc dynamic var shoppingData  = 1
    @objc dynamic var restaurantData  = 1
    @objc dynamic var swimmingData  = 1
    @objc dynamic var hospitalData  = 1
    @objc dynamic var fuelstationData  = 1
    @objc dynamic var gymData  = 1
    @objc dynamic var maleData = 1
    @objc dynamic var femaleData = 1
    @objc dynamic var noPreferenceData = 1
    @objc dynamic var petFriendlyData = 1
    @objc dynamic var noSmokingData = 1
    @objc dynamic var quitePlaceData = 1
    @objc dynamic var dateField = 00000000000.0000000000
    @objc dynamic var latitude = 00000000000.0000000000
    @objc dynamic var longitude = 00000000000.0000000000
    
    @objc dynamic var postedByID : String? = nil
    @objc dynamic var postedByFirstName : String? = nil
    @objc dynamic var postedByLastName : String? = nil
    @objc dynamic var profileImage : String? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }

    
}
extension property{
    func writeToRealm(){
        try! uiRealm.write{
            uiRealm.add(self, update: Realm.UpdatePolicy.all)
        }
    }
}
