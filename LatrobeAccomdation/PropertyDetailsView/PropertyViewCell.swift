//
//  PropertyViewCell.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 13/12/19.
//  Copyright © 2019 Navi. All rights reserved.
//

import UIKit

class PropertyViewCell: UITableViewCell {

    @IBOutlet weak var prepertyImage: UIImageView!
    @IBOutlet weak var propertyAddress: UILabel!
    @IBOutlet weak var propertyRent: UILabel!
    
    @IBOutlet weak var propertyAvailable: UILabel!
  
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var postedByLabel: UILabel!
    
    @IBOutlet weak var beds: UIButton!
    
    @IBOutlet weak var bathroom: UIButton!
    @IBOutlet weak var parking: UIButton!
    @IBOutlet weak var female: UIButton!
  
//    @IBOutlet weak var cardview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImage.layer.borderWidth = 1.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
//        cardview.layer.cornerRadius = cardview.frame.size.height/0.05
        profileImage.clipsToBounds = true
        
        
        
        
            // Make it card-like
//            containerView.layer.cornerRadius = 10
//            containerView.layer.shadowOpacity = 1
//            containerView.layer.shadowRadius = 2
//            containerView.layer.shadowColor = UIColor(named: "Orange")?.cgColor
//            containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
//            containerView.backgroundColor = UIColor(named: "Red")
//
      
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
   

    override func layoutSubviews() {
//



        }

}
