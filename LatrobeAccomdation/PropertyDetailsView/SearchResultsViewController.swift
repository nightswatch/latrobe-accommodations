//
//  SearchResultsViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 12/12/19.
//  Copyright © 2019 Navi. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import SDWebImage
import MapKit
import CoreLocation


class SearchResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var searchResultsView: UITableView!
    @IBOutlet weak var filterButton: UIButton!
    
    
    // -------------Bills Variable Data-----------
    var bill: Int = 1
    var billData: Int = 1
    // ------------property type--------
    
    var apartmentData: Int = 1
    var studioAptData: Int = 1
    var sharedRoomData: Int = 1
    var privateRoomData: Int = 1
    
    var apartment: Int = 1
    var studioApt: Int = 1
    var sharedRoom: Int = 1
    var privateRoom: Int = 1
    //    --------------Furnished Variables Data-----------------
    var bedsData : Int = 1
    var studyTableData : Int = 1
    var wardrobeData : Int = 1
    var kitchenAmenitiesData : Int = 1
    
    var beds : Int = 1
    var studyTable : Int = 1
    var wardrobe : Int = 1
    var kitchenAmenities : Int = 1
    
    //    --------------Features Variables Data-----------------
    var roomsData : Int = 1
    var bathroomsData : Int = 1
    var coolingData : Int = 1
    var wifiData : Int = 1
    var washingMachineData : Int = 1
    var heatingData : Int = 1
    var parkingData : Int = 1
    var wheelchairData : Int = 1
    
    var rooms : Int = 1
    var bathrooms : Int = 1
    var cooling : Int = 1
    var wifi : Int = 1
    var washingMachine : Int = 1
    var heating : Int = 1
    var parking : Int = 1
    var wheelchair : Int = 1
    //    --------------Accessibilities Variables Data-----------------
    var publicTransportData : Int = 1
    var storeData : Int = 1
    var shoppingData : Int = 1
    var restaurantData : Int = 1
    var swimmingData : Int = 1
    var hospitalData : Int = 1
    var fuelstationData : Int = 1
    var gymData : Int = 1
    
    var publicTransport : Int = 1
    var store : Int = 1
    var shopping : Int = 1
    var restaurant : Int = 1
    var swimming : Int = 1
    var hospital : Int = 1
    var fuelstation : Int = 1
    var gym : Int = 1
    //    --------------Tenants Variables Data-----------------
    var maleData: Int = 1
    var femaleData: Int = 1
    var noPreferenceData: Int = 1
    
    var male: Int = 1
    var female: Int = 1
    var noPreference: Int = 1
    //    --------------HomeRules Variable Data-----------------
    var petFriendlyData: Int = 1
    var noSmokingData: Int = 1
    var quitePlaceData: Int = 1
    
    var petFriendly: Int = 1
    var noSmoking: Int = 1
    var quitePlace: Int = 1
    
    var minRentValue: Int = 0
    var maxRentValue: Int = 2000
    
    
    
    var listedProperties = [propertyDetails]()
    var selectedCapmus:String = "Bundoora"
    var propertyID:String = ""
    
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animatetable()
        navigationController?.isNavigationBarHidden = false
        
        
        loadProperties()
        
        
        
    }
    
    @objc func handleModalDismissed() {
        loadProperties()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animatetable()
        loadProperties()
        navigationController?.isNavigationBarHidden = false
        
        
    }
    var result : Results<property>!
    
    
    // ---------------------------RETRIEVING DATA FROM FIREBASE--------------------------------------
    func loadProperties(){
        
        
        db.collection("Property")
            .order(by: "DateField")
            .whereField("Campus", isEqualTo: selectedCapmus)
            .addSnapshotListener
            {
                (querySnapshot, error) in
                self.listedProperties = []
                if let e = error{
                    print("There was an error in retrieving data \(e)")
                    self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                }else {
                    if let snapshotDocument = querySnapshot?.documents{
                        for doc in snapshotDocument{
                            let data = doc.data()
                            if let propertyAddress = data["Address"] as? String,
                                let propertyAvailable = data["AvailableFrom"] as? String,
                                //                                let propertyTitleData = data["propertyTitle"] as? String,
                                let propertyRent = data["Rent"] as? Double,
                                let propertyID = data["ID"] as? String,
                                let propertyImageData = data["PropertyImage"]as? String,
                                let propertyCampus = data["Campus"]as? String,
                                let apartmentData = data["Apartment"] as? Int,
                                let studioAptData = data["StudioApartment"] as? Int,
                                let sharedRoomData = data["SharedRoom"] as? Int,
                                let privateRoomData = data["PrivateRoom"] as? Int,
                                let bedsData = data["Beds"] as? Int,
                                let studyTableData = data["StudyTabel"] as? Int,
                                let wardrobeData = data["Wardrobe"] as? Int,
                                let kitchenAmenitiesData = data["KitchenAmenities"] as? Int,
                                let roomsData = data["Rooms"] as? Int,
                                let bathroomsData = data["Bathrooms"] as? Int,
                                let coolingData = data["Cooling"] as? Int,
                                let wifiData = data["Wifi"] as? Int,
                                let washingMachineData = data["WashingMachine"] as? Int,
                                let heatingData = data["Heating"] as? Int,
                                let parkingData = data["Parking"] as? Int,
                                let wheelchairData = data["WheelChair"] as? Int,
                                let publicTransportData = data["PublicTransport"] as? Int,
                                let storeData = data["Store"] as? Int,
                                let shoppingData = data["Shopping"] as? Int,
                                let restaurantData = data["Restaurant"] as? Int,
                                let swimmingData = data["Swimming"] as? Int,
                                let hospitalData = data["Hospital"] as? Int,
                                let fuelstationData = data["FuelStation"] as? Int,
                                let gymData = data["Gym"] as? Int,
                                let maleData = data["Male"] as? Int,
                                let femaleData = data["Female"] as? Int,
                                let noPreferenceData = data["NoPreference"] as? Int,
                                let petFriendlyData = data["PetFriendly"] as? Int,
                                let noSmokingData = data["NoSmoking"] as? Int,
                                let quitePlaceData = data["QuitePlace"] as? Int,
                                let latitude = data["Latitude"] as? Double,
                                let longitude = data["Longitude"] as? Double,
                                let dateField = data["DateField"] as? Double,
                                let postedByID = data["PostedBy"] as? String,
                                let propertyBill = data["Bill"]as? Int{
                                
                                self.db.collection("User").whereField("ID", isEqualTo: postedByID).addSnapshotListener { (querySnapshot, error) in
                                    //                                                  self.initializePage()
                                    if let e = error{
                                        print("There was an error in retrieving data \(e)")
                                        self.alertNotification(Title: "Error!", Message: "Error in retrieveing data from FireStore")
                                    }else {
                                        if let snapshotDocument = querySnapshot?.documents{
                                            for doc in snapshotDocument{
                                                let data = doc.data()
                                                if let profileImage = data["ProfilePicture"] as? String,
                                                    let postedByFirstName = data["FirstName"] as? String,
                                                    let postedByLastName = data["LastName"] as? String{
                                                    
                                                    //                            let propertyImageGet = UIImage(data: propertyImageData)
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    var propertyRealm = property()
                                                    //
                                                    //                                                    propertyRealm.propertyTitle = propertyTitleData
                                                    propertyRealm.availableFromTextField = propertyAvailable
                                                    propertyRealm.address = propertyAddress
                                                    propertyRealm.selectedCampus = propertyCampus
                                                    propertyRealm.id = propertyID
                                                    propertyRealm.imageData = propertyImageData
                                                    //                                propertyRealm.image = propertyImage
                                                    //                                                    propertyBill.bill = Int(propertyBill)
                                                    propertyRealm.rent = Int(propertyRent)
                                                    propertyRealm.apartmentData = apartmentData
                                                    propertyRealm.studioAptData = studioAptData
                                                    propertyRealm.sharedRoomData = sharedRoomData
                                                    propertyRealm.privateRoomData = privateRoomData
                                                    propertyRealm.bedsData = bedsData
                                                    propertyRealm.studyTableData = studyTableData
                                                    propertyRealm.wardrobeData = wardrobeData
                                                    propertyRealm.kitchenAmenitiesData = kitchenAmenitiesData
                                                    propertyRealm.roomsData = roomsData
                                                    propertyRealm.bathroomsData = bathroomsData
                                                    propertyRealm.coolingData = coolingData
                                                    propertyRealm.wifiData = wifiData
                                                    propertyRealm.washingMachineData = washingMachineData
                                                    propertyRealm.heatingData = heatingData
                                                    propertyRealm.parkingData = parkingData
                                                    propertyRealm.wheelchairData = wheelchairData
                                                    propertyRealm.publicTransportData = publicTransportData
                                                    propertyRealm.storeData = storeData
                                                    propertyRealm.shoppingData = shoppingData
                                                    propertyRealm.restaurantData = restaurantData
                                                    propertyRealm.swimmingData = swimmingData
                                                    propertyRealm.hospitalData = hospitalData
                                                    propertyRealm.fuelstationData = fuelstationData
                                                    propertyRealm.gymData = gymData
                                                    propertyRealm.maleData = maleData
                                                    propertyRealm.femaleData = femaleData
                                                    propertyRealm.noPreferenceData = noPreferenceData
                                                    propertyRealm.petFriendlyData = petFriendlyData
                                                    propertyRealm.noSmokingData = noSmokingData
                                                    propertyRealm.quitePlaceData = quitePlaceData
                                                    propertyRealm.dateField = dateField
                                                    propertyRealm.latitude = latitude
                                                    propertyRealm.longitude = longitude
                                                    propertyRealm.postedByID = postedByID
                                                    propertyRealm.postedByFirstName = postedByFirstName
                                                    propertyRealm.postedByLastName = postedByLastName
                                                    propertyRealm.profileImage = profileImage
                                                    propertyRealm.writeToRealm()
                                                    
                                                    //                                self.listedProperties.append(propertyFirebase)
                                                    
                                                    
                                                    DispatchQueue.main.async {
                                                        self.reloadData()
                                                    }
                                                    
                                                }
                                            }
                                            
                                            
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                }
        }
    }
    
    func reloadData(){
        result = uiRealm.objects(property.self).sorted(byKeyPath: "dateField", ascending: false)
            .filter("selectedCampus == %@", selectedCapmus)
            .filter("rent <= %@", maxRentValue)
            .filter("rent >= %@", minRentValue)
            .filter("apartmentData <= %@",apartmentData)
            .filter("studioAptData <= %@",studioAptData)
            .filter("sharedRoomData <= %@",sharedRoomData)
            .filter("privateRoomData <= %@",privateRoomData)
            .filter("bedsData <= %@",bedsData)
            .filter("studyTableData <= %@",studyTableData)
            .filter("wardrobeData <= %@",wardrobeData)
            .filter("kitchenAmenitiesData <= %@",kitchenAmenitiesData)
            .filter("roomsData <= %@",roomsData)
            .filter("bathroomsData <= %@",bathroomsData)
            .filter("coolingData <= %@",coolingData)
            .filter("wifiData <= %@",wifiData)
            .filter("washingMachineData <= %@",washingMachineData)
            .filter("parkingData <= %@",parkingData)
            .filter("wheelchairData <= %@",wheelchairData)
            .filter("publicTransportData <= %@",publicTransportData)
            .filter("storeData <= %@",storeData)
            .filter("shoppingData <= %@",shoppingData)
            .filter("restaurantData <= %@",restaurantData)
            .filter("swimmingData <= %@",swimmingData)
            .filter("hospitalData <= %@",hospitalData)
            .filter("fuelstationData <= %@",fuelstationData)
            .filter("gymData <= %@",gymData)
            .filter("maleData <= %@",maleData)
            .filter("femaleData <= %@",femaleData)
            .filter("noPreferenceData <= %@",noPreferenceData)
            .filter("petFriendlyData <= %@",petFriendlyData)
            .filter("noSmokingData <= %@",noSmokingData)
            .filter("quitePlaceData <= %@",quitePlaceData)
        
        self.searchResultsView.reloadData()
    }
    
    
    
    //----------------------------NUMBER OF ROWS IN THE TABELVIEW--------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if result != nil {
            //            return (result.count + 1)
            return (result.count)
        }else{
            return 0
        }
        
    }
    
    //------------------------WHICH CELL SHOULD TAKE WHICH ROW-------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let row = indexPath.row
        //        let intitialValue = 1
        //        let finalValue = result.count + 1
        //        switch row  {
        //
        //        case (intitialValue...finalValue):
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "propertListCell", for: indexPath)as! PropertyViewCell
        //            cell.profileImage?.sd_setImage(with: URL(string: self.result[indexPath.row - 1].profileImage!))
        //            cell.postedByLabel.text = "Posted by \(result[indexPath.row - 1].postedByFirstName!)"
        //            cell.propertyAddress?.text = result[indexPath.row - 1].address
        //            cell.propertyRent?.text = "$\(String(result[indexPath.row - 1].rent)) Per Week"
        //            cell.prepertyImage?.sd_setImage(with: URL(string: self.result[indexPath.row - 1].imageData!))
        //            let bedsData = result[indexPath.row - 1].bedsData
        //            if bedsData == 1{
        //                cell.beds.isEnabled = false
        //            }else{
        //                cell.beds.isEnabled = true
        //            }
        //
        //            return cell
        //        default:
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "mapViewCell", for: indexPath)as! UITableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "propertListCell", for: indexPath)as! PropertyViewCell
        cell.profileImage?.sd_setImage(with: URL(string: self.result[indexPath.row].profileImage!))
        cell.postedByLabel.text = "Posted by \(result[indexPath.row].postedByFirstName!)"
        cell.propertyAddress?.text = result[indexPath.row].address
        //        cell.propertyTitleData?.text = result[indexPath.row].propertyTitle
        if result[indexPath.row].availableFromTextField != "" {
            cell.propertyAvailable?.text = "Avail. from \(result[indexPath.row].availableFromTextField!)"
        } else {
            cell.propertyAvailable?.text = "Available Now"
        }
        //        if result[indexPath.row].availableFromTextField == "Available Now"
        
        cell.propertyRent?.text = "$\(String(result[indexPath.row].rent)) Per Week"
        //        cell.propertyBill?.text = result[indexPath.row]
        cell.prepertyImage?.sd_setImage(with: URL(string: self.result[indexPath.row].imageData!))
        let bedsData = result[indexPath.row].bedsData
        let bathroomsData = result[indexPath.row].bathroomsData
        let parkingData = result[indexPath.row].parkingData
        let femaleData = result[indexPath.row].femaleData
        if bedsData == 1{
            cell.beds.isEnabled = false
        }else{
            cell.beds.isEnabled = true
        }
        if bathroomsData == 1{
            cell.bathroom.isEnabled = false
        }else{
            cell.bathroom.isEnabled = true
        }
        if parkingData == 1{
            cell.parking.isEnabled = false
        }else{
            cell.parking.isEnabled = true
        }
        if femaleData == 1{
            cell.female.isEnabled = false
        }else{
            cell.female.isEnabled = true
        }
        
        return cell
    }
    
    
    
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        propertyID = result[indexPath.row].id!
        //        print("UUID = \(propertyID)")
        apartment = result[indexPath.row].apartmentData
        studioApt = result[indexPath.row].studioAptData
        sharedRoom = result[indexPath.row].sharedRoomData
        privateRoom = result[indexPath.row].privateRoomData
        
        beds = result[indexPath.row].bedsData
        studyTable = result[indexPath.row].studyTableData
        wardrobe = result[indexPath.row].wardrobeData
        kitchenAmenities = result[indexPath.row].kitchenAmenitiesData
        
        
        male = result[indexPath.row].maleData
        female = result[indexPath.row].femaleData
        noPreference = result[indexPath.row].noPreferenceData
        
        petFriendly = result[indexPath.row].petFriendlyData
        noSmoking = result[indexPath.row].noSmokingData
        quitePlace = result[indexPath.row].quitePlaceData
        
        rooms  = result[indexPath.row].roomsData
        bathrooms = result[indexPath.row].bathroomsData
        cooling = result[indexPath.row].coolingData
        wifi = result[indexPath.row].wifiData
        washingMachine = result[indexPath.row].washingMachineData
        heating = result[indexPath.row].heatingData
        parking = result[indexPath.row].parkingData
        wheelchair = result[indexPath.row].wheelchairData
        
        publicTransport  = result[indexPath.row].publicTransportData
        store  = result[indexPath.row].storeData
        shopping  = result[indexPath.row].shoppingData
        restaurant  = result[indexPath.row].restaurantData
        swimming  = result[indexPath.row].swimmingData
        hospital  = result[indexPath.row].hospitalData
        fuelstation  = result[indexPath.row].fuelstationData
        gym  = result[indexPath.row].gymData
        
        
        performSegue(withIdentifier: "toDetailsVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailsVC" {
            let destinationVC = segue.destination as! PropertyDetailViewController
            destinationVC.propertyID = propertyID
            
            destinationVC.apartmentData = apartment
            destinationVC.studioAptData = studioApt
            destinationVC.sharedRoomData = sharedRoom
            destinationVC.privateRoomData = privateRoom
            
            destinationVC.bedsData = beds
            destinationVC.studyTableData = studyTable
            destinationVC.wardrobeData = wardrobe
            destinationVC.kitchenAmenitiesData = kitchenAmenities
            
            destinationVC.publicTransportData = publicTransport
            destinationVC.storeData = store
            destinationVC.shoppingData = shopping
            destinationVC.restaurantData = restaurant
            destinationVC.swimmingData = swimming
            destinationVC.hospitalData = hospital
            destinationVC.fuelstationData = fuelstation
            destinationVC.gymData = gym
            
            destinationVC.maleData = male
            destinationVC.femaleData = female
            destinationVC.noPreferenceData = noPreference
            
            destinationVC.petFriendlyData = petFriendly
            destinationVC.noSmokingData = noSmoking
            destinationVC.quitePlaceData = quitePlace
            
            destinationVC.roomsData = rooms
            destinationVC.bathroomsData = bathrooms
            destinationVC.coolingData = cooling
            destinationVC.wifiData = wifi
            destinationVC.washingMachineData = washingMachine
            destinationVC.heatingData = heating
            destinationVC.parkingData = parking
            destinationVC.wheelchairData = wheelchair
            
        }
    }
    
    @IBAction func filterButtonPressed(_ sender: Any) {
    }
    
    func alertNotification(Title : String, Message : String){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func unwindToViewController(segue: UIStoryboardSegue) {
        let source = segue.source as? FiltersViewController
        loadProperties()
    }
    
    
    func animatetable(){
        searchResultsView.reloadData()
        let cells = searchResultsView.visibleCells
        let tableViewHeight = searchResultsView.bounds.size.height
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        var delayCounter = 0
        for cell in cells{
            UIView.animate(withDuration: 1.75 * 2, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
            
        }
    }
    
    
    
}
