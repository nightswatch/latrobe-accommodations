//
//  propertyDetails.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 13/12/19.
//  Copyright © 2019 Navi. All rights reserved.
//

import Foundation

import UIKit

class propertyDetails: NSObject {
    
    var address : String
    var rent : Double
    var imageData : String
    var campus : String
    var id : String
    
    var latitude : Double
    var longitude : Double
    var dateField : Double
    
    var apartmentData: Int
    var studioAptData: Int
    var sharedRoomData: Int
    var privateRoomData: Int
    var bedsData : Int
    var studyTableData : Int
    var wardrobeData : Int
    var kitchenAmenitiesData : Int
    var roomsData : Int
    var bathroomsData : Int
    var coolingData : Int
    var wifiData : Int
    var washingMachineData : Int
    var heatingData : Int
    var parkingData : Int
    var wheelchairData : Int
    var publicTransportData : Int
    var storeData : Int
    var shoppingData : Int
    var restaurantData : Int
    var swimmingData : Int
    var hospitalData : Int
    var fuelstationData : Int
    var gymData : Int
    var maleData: Int
    var femaleData: Int
    var noPreferenceData: Int
    var petFriendlyData: Int
    var noSmokingData: Int
    var quitePlaceData: Int
    
    var profileImage: String
    var postedbyFirstName: String
    var postedbyLastName: String
    var postedbyID: String
    var availableFromTextField: String
    var availableDate: Date
//    var propertyTitle: String
    
    init(propertyAddress: String?, propertyRent:Double?, propertyImage:String?, propertyCampus:String?, propertyID: String?,propertyapartmentData: Int?, propertystudioAptData: Int?, propertysharedRoomData: Int?, propertyprivateRoomData: Int?, propertybedsData : Int?, propertystudyTableData : Int?, propertywardrobeData : Int?, propertykitchenAmenitiesData : Int?, propertyroomsData : Int?, propertybathroomsData : Int?, propertycoolingData : Int?, propertywifiData : Int?, propertywashingMachineData : Int?, propertyheatingData : Int?, propertyparkingData : Int?, propertywheelchairData : Int?, propertypublicTransportData : Int?, propertystoreData : Int?, propertyshoppingData : Int?, propertyrestaurantData : Int?, propertyswimmingData : Int?, propertyhospitalData : Int?, propertyfuelstationData : Int?, propertygymData : Int?, propertymaleData: Int?, propertyfemaleData: Int?, propertynoPreferenceData: Int?, propertypetFriendlyData: Int?, propertynoSmokingData: Int?, propertyquitePlaceData: Int?, propertylatitude: Double?, propertylongitude : Double?, propertydateField : Double?, postedByImage: String?, postedByFirstName: String?, postedByLastName: String?, postedByID: String?, propertyAvailable: String?, propertyDate: Date?) {
        
       availableDate = propertyDate!
        availableFromTextField = propertyAvailable!
        address = propertyAddress!
        rent = propertyRent!
        imageData = propertyImage!
        campus = propertyCampus!
        id = propertyID!
        apartmentData = propertyapartmentData!
        studioAptData = propertystudioAptData!
        sharedRoomData = propertysharedRoomData!
        privateRoomData = propertyprivateRoomData!
        bedsData  = propertybedsData!
        studyTableData  = propertystudyTableData!
        wardrobeData  = propertywardrobeData!
        kitchenAmenitiesData  = propertykitchenAmenitiesData!
        roomsData  = propertyroomsData!
        bathroomsData  = propertybathroomsData!
        coolingData  = propertycoolingData!
        wifiData  = propertywifiData!
        washingMachineData  = propertywashingMachineData!
        heatingData  = propertyheatingData!
        parkingData  = propertyparkingData!
        wheelchairData  = propertywheelchairData!
        publicTransportData  = propertypublicTransportData!
        storeData  = propertystoreData!
        shoppingData  = propertyshoppingData!
        restaurantData  = propertyrestaurantData!
        swimmingData  = propertyswimmingData!
        hospitalData  = propertyhospitalData!
        fuelstationData  = propertyfuelstationData!
        gymData  = propertygymData!
        maleData = propertymaleData!
        femaleData = propertyfemaleData!
        noPreferenceData = propertynoPreferenceData!
        petFriendlyData = propertypetFriendlyData!
        noSmokingData = propertynoSmokingData!
        quitePlaceData = propertyquitePlaceData!
        latitude  = propertylatitude!
        longitude  = propertylongitude!
        dateField  = propertydateField!
        profileImage = postedByImage!
        postedbyFirstName = postedByFirstName!
        postedbyLastName = postedByLastName!
        postedbyID = postedByID!
        
    }
    
}
