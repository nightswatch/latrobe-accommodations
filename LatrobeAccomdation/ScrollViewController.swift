//
//  ScrollViewController.swift
//  LatrobeAccomdation
//
//  Created by Naveen Nataraj on 10/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit

class ScrollViewController: UIViewController {

    @IBOutlet weak var downarrow1: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func downArrow1pressed(_ sender: Any) {
        let desiredOffset = CGPoint(x: 0, y: 780)
        self.scrollView.setContentOffset(desiredOffset, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
