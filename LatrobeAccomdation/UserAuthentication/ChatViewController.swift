//
//  ChatViewController.swift
//  LatrobeAccomdation
//
//  Created by lorena munoz on 15/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase

class ChatViewController: UIViewController {

    
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var messageView: UIView!
    
    @IBOutlet weak var messageTextField: UITextField!
    
       
        let db = Firestore.firestore()

        
        var messages: [Message] = []
        
        
        override func viewDidLoad() {
              super.viewDidLoad()
              //tableView.delegate = self
            chatTableView.dataSource = self
              // APPEAR TITTLE//
            title = "La Trobe Accommodation"
             // HIDE BACK BUTTON//
            navigationItem.hidesBackButton = true
            
            chatTableView.register(UINib(nibName: K.cellNibName, bundle: nil), forCellReuseIdentifier: K.cellIdentifier)
                
                loadMessages()
                
            }
            
            func loadMessages() {
                //ORDER OF MESSAGES//
                db.collection(K.FStore.collectionName)
                    .order(by: K.FStore.dateField)
                    .addSnapshotListener { (querySnapshot, error) in
                    
                    self.messages = []
                    
                    if let e = error {
                        print("There was an issue retrieving data from Firestore. \(e)")
                    } else {
                        if let snapshotDocuments = querySnapshot?.documents {
                            for doc in snapshotDocuments {
                                let data = doc.data()
                                if let messageSender = data[K.FStore.senderField] as? String, let messageBody = data[K.FStore.bodyField] as? String {
                                    let newMessage = Message(sender: messageSender, body: messageBody)
                                    self.messages.append(newMessage)
                                    
                                    DispatchQueue.main.async {
                                           self.chatTableView.reloadData()
                                        let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
                                        self.chatTableView.scrollToRow(at: indexPath, at: .top, animated: false)
                                    }
                                }
                            }
                        }
                    }
                }
            }
     
        
        
        @IBAction func sendPressed(_ sender: Any) {
         
               if let messageBody = messageTextField.text, let messageSender = Auth.auth().currentUser?.email {
                   db.collection(K.FStore.collectionName).addDocument(data: [
                       K.FStore.senderField: messageSender,
                       K.FStore.bodyField: messageBody,
                       K.FStore.dateField: Date().timeIntervalSince1970
                   ]) { (error) in
                       if let e = error {
                           print("There was an issue saving data to firestore, \(e)")
                       } else {
                           print("Successfully saved data.")
                           
                           DispatchQueue.main.async {
                                self.messageTextField.text = ""
                           }
                       }
                   }
               }
           }
           
        
        @IBAction func cameraPressed(_ sender: Any) {
        }
        
        
        @IBAction func picturePressed(_ sender: Any) {
        }
        
        
        @IBAction func logoutPressed(_ sender: Any) {
        
            let firebaseAuth = Auth.auth()
            do {
             try Auth.auth().signOut()
                //LOGOUT BUTTON IS GOING TO THE ROOTVIEWCONTROLLER//
                navigationController?.popToRootViewController( animated: true)
                
            } catch let signOutError as NSError {
              print ("Error signing out: %@", signOutError)
            }
              
        }
        
        
    }
        
        

    extension ChatViewController: UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return messages.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let message = messages[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: K.cellIdentifier, for: indexPath) as! MessageCell
            cell.label.text = message.body
            //This is a message from the current user.
                   if message.sender == Auth.auth().currentUser?.email {
                       cell.leftImageView.isHidden = true
                       cell.rightImageView.isHidden = false
                       cell.messageBubble.backgroundColor = UIColor(named: K.BrandColors.red)
                       cell.label.textColor = UIColor(named: K.BrandColors.black)
                   }
                   //This is a message from another sender.
                   else {
                       cell.leftImageView.isHidden = false
                       cell.rightImageView.isHidden = true
                       cell.messageBubble.backgroundColor = UIColor(named: K.BrandColors.red)
                       cell.label.textColor = UIColor(named: K.BrandColors.lightRed)
                   }
        
        
            
            
            
            
           
            
          
          
            return cell
        }
    }

