//
//  LogInViewController.swift
//  LatrobeAccomdation
//
//  Created by lorena munoz on 15/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase

class LogInViewController: UIViewController {
    
    //LOG IN A USER WITH AN EMAIL ADDRESS AND PASSWORD//
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    override func viewDidLoad() {
        activity.hidesWhenStopped = true
        logInButton.layer.cornerRadius = 10
        backgroundImage.isOpaque = true
//        emailTextField.layer.borderColor = UIColor.black as! CGColor
        emailTextField.layer.borderColor = #colorLiteral(red: 0, green: 0.5994044542, blue: 0.5334758759, alpha: 1)
        emailTextField.layer.borderWidth = 2
        passwordTextField.layer.borderColor = #colorLiteral(red: 0, green: 0.5994044542, blue: 0.5334758759, alpha: 1)
        passwordTextField.layer.borderWidth = 2
    }
    
    
    
    @IBAction func loginPressed(_ sender: Any) {
        
        activity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        if let email = emailTextField.text, let password = passwordTextField.text {
            
                 Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
                 if let e = error {
                     
                     print (e.localizedDescription)
                     alertNotification(Title: "Check your email you have entered", Message: e.localizedDescription)
                     self.activity.stopAnimating()
                    self.emailTextField.text = ""
                    self.passwordTextField.text = ""
                           }else {
                    
                    self.dismiss(animated: true, completion: nil)
                    self.activity.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
             //NAVIGATE TO THE CHAT VIEW CONTROLLER//
//                 self.performSegue(withIdentifier: "LoginToChat", sender: self)
                     }
                     }
                     func alertNotification(Title : String, Message : String){
                     
                     let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
                     let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                     alert.addAction(okButton)
                     self.present(alert, animated: true, completion: nil)
                         
                     }
                 }
             }
         }
