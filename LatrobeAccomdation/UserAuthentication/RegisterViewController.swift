//
//  RegisterViewController.swift
//  LatrobeAccomdation
//
//  Created by lorena munoz on 15/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //REGISTER USER AND A PASSWORD-BASED ON ACCOUNT-FIREBASE//
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var createAccountButton: UIButton!
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
          activity.hidesWhenStopped = true
        createAccountButton.layer.cornerRadius = 10
         profileImage.layer.borderWidth = 1.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.clipsToBounds = true
        
        profileImage.isUserInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(chooseImage))
        profileImage.addGestureRecognizer(gestureRecognizer)
        
      }
    
    @objc func chooseImage(){
        
        let pickerContorller = UIImagePickerController()
        pickerContorller.delegate = self
        pickerContorller.sourceType = .photoLibrary
        present(pickerContorller, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        profileImage.image = info[.originalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func createAccountPressed(_ sender: Any) {
        activity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                if let e = error {
                    print (e.localizedDescription)
                    self.alertNotification(Title: "User Not Registered!", Message: e.localizedDescription)
                    self.activity.stopAnimating()
                }else {
                    let storage = Storage.storage()
                           let storageReference = storage.reference()
                           let mediaFolder = storageReference.child("media")
                    if let data = self.profileImage.image?.jpegData(compressionQuality: 0.5) {
                               let uuid = UUID().uuidString
                               let imageReference = mediaFolder.child("\(uuid).jpg")
                               imageReference.putData(data, metadata: nil) { (metadata, error) in
                                   if error != nil {
                                    self.alertNotification(Title: "Error!", Message: error?.localizedDescription ?? "Error")
                                   } else {
                                       imageReference.downloadURL { (url, error) in
                                           if error == nil {
                                               let imageUrl = url?.absoluteString
                                            if let userID = self.emailTextField.text,
                                                let firstName = self.firstName.text,
                                            let lastName = self.lastName.text
                                               {
                                                 
                                                   self.db.collection("User").addDocument(data: ["ID" : userID,
                                                                                                     "FirstName" : firstName,
                                                                                                     "LastName" : lastName,
                                                                                                     "ProfilePicture" : imageUrl])
                                                   { (error) in
                                                       if let e = error{
                                                           print("There was an isssue saving the User Details, \(e)")
                                                        self.alertNotification(Title: "Not Posted!", Message: "Not saved to the database")
                                                       }
                                                       else{
                                                           print("Successfully saved the User Details")
                                                        self.dismiss(animated: true, completion: nil)
                    self.activity.stopAnimating()
                                                        UIApplication.shared.endIgnoringInteractionEvents()
                                                    }}}}}}}}}}}}
                                                
            
            func alertNotification(Title : String, Message : String){
                
                let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
                let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
            }
            
            
            // Do any additional setup after loading the view.
        }
        
        
        

    
    

