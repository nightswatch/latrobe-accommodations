//
//  WelcomeViewController.swift
//  LatrobeAccomdation
//
//  Created by lorena munoz on 15/1/20.
//  Copyright © 2020 Navi. All rights reserved.
//

import UIKit

 class WelcomeViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         navigationController?.isNavigationBarHidden = true
     }

     override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
         navigationController?.isNavigationBarHidden = false
     }

     override func viewDidLoad() {
         super.viewDidLoad()
        signInButton.layer.cornerRadius = 10
        registerButton.layer.cornerRadius = 10
//         titleLabel.text = ""
//         var charIndex = 0.0
//         let titleText = K.appName
//         for letter in titleText {
//             Timer.scheduledTimer(withTimeInterval: 0.1 * charIndex, repeats: false) { (timer) in
//                 self.titleLabel.text?.append(letter)
//             }
//             charIndex += 1
         }

     }


 

